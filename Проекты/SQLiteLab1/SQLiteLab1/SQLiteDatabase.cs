﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Windows.Forms;

namespace SQLiteLab1
{
    internal class SqLiteDatabase
    {
        private readonly string _dbConnection;

        /// <summary>
        ///     Конструктор для подключения к базе, которая находится в папке проекта
        /// </summary>
        public SqLiteDatabase()
        {
            _dbConnection = @"Data Source=|DataDirectory|\Museum.sqlite";
        }

        /// <summary>
        ///     Конструктор для подключения к базе, которая находится не в папке проекта
        /// </summary>
        /// <param name="inputFile">Путь к файлу</param>
        public SqLiteDatabase(string inputFile)
        {
            _dbConnection = string.Format("Data Source={0}", inputFile);
        }

        /// <summary>
        ///     Конструктор с указанием дополнительных параметров соединения
        /// </summary>
        /// <param name="connectionOpts">Словарь, содержащий все необходимые параметры и их значения</param>
        public SqLiteDatabase(Dictionary<string, string> connectionOpts)
        {
            var str = connectionOpts.Aggregate("",
                (current, row) => current + string.Format("{0}={1}; ", row.Key, row.Value));
            str = str.Trim().Substring(0, str.Length - 1);
            _dbConnection = str;
        }

        /// <summary>
        ///     Позволяет выполнить запрос к БД
        /// </summary>
        /// <param name="sql">SQL-запрос на выполнение</param>
        /// <returns>Объект DataTable, содержащий результирующий набор</returns>
        public DataTable GetDataTable(string sql)
        {
            var dt = new DataTable();
            SQLiteDataReader reader = null;
            SQLiteConnection con = null;
            try
            {
                con = new SQLiteConnection(_dbConnection);
                con.Open();
                var mycommand = new SQLiteCommand(con) {CommandText = sql};
                reader = mycommand.ExecuteReader();
                dt.Load(reader);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (con != null)
                    con.Close();
            }
            return dt;
        }

        /// <summary>
        ///     Позволяет взаимодействовать с БД для определения количества изменяемых записей
        /// </summary>
        /// <param name="sql">SQL-запрос на выполнение</param>
        /// <returns>Целое число, содержащее количество обновленных строк</returns>
        public int ExecuteNonQuery(string sql)
        {
            SQLiteConnection con = null;
            int rowsUpdated;
            try
            {
                con = new SQLiteConnection(_dbConnection);
                con.Open();
                var mycommand = new SQLiteCommand(con) {CommandText = sql};
                rowsUpdated = mycommand.ExecuteNonQuery();
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return rowsUpdated;
        }

        /// <summary>
        ///     Позволяет извлекать отдельные элементы из БД.
        /// </summary>
        /// <param name="sql">SQL-запрос на выполнение</param>
        /// <returns>Результат запроса</returns>
        public string ExecuteScalar(string sql)
        {
            SQLiteConnection con = null;
            object value;
            try
            {
                con = new SQLiteConnection(_dbConnection);
                con.Open();
                var mycommand = new SQLiteCommand(con) {CommandText = sql};
                value = mycommand.ExecuteScalar();
            }
            finally
            {
                if (con != null)
                    con.Close();
            }
            return value != null ? value.ToString() : "";
        }

        /// <summary>
        ///     Позволяет обновлять строки в БД
        /// </summary>
        /// <param name="tableName">Таблица для обновления</param>
        /// <param name="data">Словарь, содержащий имена столбцов и их новые значения</param>
        /// <param name="where">Предложение Where в инструкции Update</param>
        /// <returns>Булево значение True или false для указания успеха или неудачи</returns>
        public bool Update(string tableName, Dictionary<string, string> data, string where)
        {
            var vals = "";
            var returnCode = true;
            if (data.Count >= 1)
            {
                vals = data.Aggregate(vals,
                    (current, val) => current + string.Format(" {0} = '{1}',", val.Key, val.Value));
                vals = vals.Substring(0, vals.Length - 1);
            }
            try
            {
                ExecuteNonQuery(string.Format("update {0} set {1} where {2};", tableName, vals, where));
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Позволяет удалять строки в БД
        /// </summary>
        /// <param name="tableName">Таблица, в которой будет выполняться запрос</param>
        /// <param name="where">Предложение Where в инструкции Delete</param>
        /// <returns>Булево значение True или false для указания успеха или неудачи</returns>
        public bool Delete(string tableName, string where)
        {
            var returnCode = true;
            try
            {
                ExecuteNonQuery(string.Format("delete from {0} where {1};", tableName, where));
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Позволяет вставить строку в БД
        /// </summary>
        /// <param name="tableName">Таблица, в которой будет выполняться запрос</param>
        /// <param name="data">Словарь, содержащий имена столбцов и данные для новой записи</param>
        /// <returns>Булево значение True или false для указания успеха или неудачи</returns>
        public bool Insert(string tableName, Dictionary<string, string> data)
        {
            var columns = "";
            var values = "";
            var returnCode = true;
            foreach (var val in data)
            {
                columns += string.Format(" {0},", val.Key);
                values += string.Format(" '{0}',", val.Value);
            }
            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);
            try
            {
                ExecuteNonQuery(string.Format("insert into {0}({1}) values({2});", tableName, columns, values));
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                returnCode = false;
            }
            return returnCode;
        }

        /// <summary>
        ///     Позволяет удалить все пользовательские таблицы в БД
        /// </summary>
        /// <returns>Булево значение True или false для указания успеха или неудачи</returns>
        public bool ClearDb()
        {
            try
            {
                var tables = GetDataTable("select NAME from SQLITE_MASTER where type='table' order by NAME;");
                foreach (DataRow table in tables.Rows)
                {
                    ClearRows(table["NAME"].ToString());
                }
                return true;
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
                return false;
            }
        }

        /// <summary>
        ///     Позволяет удалить все записи в таблице
        /// </summary>
        /// <param name="table">Имя таблицы для удаления записей</param>
        /// <returns>Булево значение True или false для указания успеха или неудачи</returns>
        public bool ClearRows(string table)
        {
            try
            {
                ExecuteNonQuery(string.Format("delete from {0};", table));
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        ///     Позволяет удалить все записи, соответствующие условию, в таблице
        /// </summary>
        /// <param name="table">Имя таблицы для удаления записей</param>
        /// <param name="cause">Условие удаления</param>
        /// <returns>Булево значение True или false для указания успеха или неудачи</returns>
        public bool ClearRowsWithCause(string table, string cause)
        {
            try
            {
                ExecuteNonQuery(string.Format("delete from {0} where {1};", table, cause));
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}