﻿namespace SQLiteLab1
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxGuide = new System.Windows.Forms.GroupBox();
            this.buttonEmployees = new System.Windows.Forms.Button();
            this.groupBoxOperationInfo = new System.Windows.Forms.GroupBox();
            this.buttonExcursions = new System.Windows.Forms.Button();
            this.buttonExhibits = new System.Windows.Forms.Button();
            this.buttonHalls = new System.Windows.Forms.Button();
            this.groupBoxGuide.SuspendLayout();
            this.groupBoxOperationInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxGuide
            // 
            this.groupBoxGuide.Controls.Add(this.buttonEmployees);
            this.groupBoxGuide.Location = new System.Drawing.Point(39, 24);
            this.groupBoxGuide.Name = "groupBoxGuide";
            this.groupBoxGuide.Size = new System.Drawing.Size(200, 100);
            this.groupBoxGuide.TabIndex = 0;
            this.groupBoxGuide.TabStop = false;
            this.groupBoxGuide.Text = "Довідники";
            // 
            // buttonEmployees
            // 
            this.buttonEmployees.Location = new System.Drawing.Point(55, 44);
            this.buttonEmployees.Name = "buttonEmployees";
            this.buttonEmployees.Size = new System.Drawing.Size(75, 23);
            this.buttonEmployees.TabIndex = 0;
            this.buttonEmployees.Text = "Працівники";
            this.buttonEmployees.UseVisualStyleBackColor = true;
            this.buttonEmployees.Click += new System.EventHandler(this.buttonEmployees_Click);
            // 
            // groupBoxOperationInfo
            // 
            this.groupBoxOperationInfo.Controls.Add(this.buttonExcursions);
            this.groupBoxOperationInfo.Controls.Add(this.buttonExhibits);
            this.groupBoxOperationInfo.Controls.Add(this.buttonHalls);
            this.groupBoxOperationInfo.Location = new System.Drawing.Point(39, 139);
            this.groupBoxOperationInfo.Name = "groupBoxOperationInfo";
            this.groupBoxOperationInfo.Size = new System.Drawing.Size(200, 160);
            this.groupBoxOperationInfo.TabIndex = 1;
            this.groupBoxOperationInfo.TabStop = false;
            this.groupBoxOperationInfo.Text = "Оперативна інформація";
            // 
            // buttonExcursions
            // 
            this.buttonExcursions.Location = new System.Drawing.Point(55, 106);
            this.buttonExcursions.Name = "buttonExcursions";
            this.buttonExcursions.Size = new System.Drawing.Size(75, 23);
            this.buttonExcursions.TabIndex = 2;
            this.buttonExcursions.Text = "Екскурсії";
            this.buttonExcursions.UseVisualStyleBackColor = true;
            this.buttonExcursions.Click += new System.EventHandler(this.buttonExcursion_Click);
            // 
            // buttonExhibits
            // 
            this.buttonExhibits.Location = new System.Drawing.Point(55, 77);
            this.buttonExhibits.Name = "buttonExhibits";
            this.buttonExhibits.Size = new System.Drawing.Size(75, 23);
            this.buttonExhibits.TabIndex = 1;
            this.buttonExhibits.Text = "Експонати";
            this.buttonExhibits.UseVisualStyleBackColor = true;
            this.buttonExhibits.Click += new System.EventHandler(this.buttonExhibits_Click);
            // 
            // buttonHalls
            // 
            this.buttonHalls.Location = new System.Drawing.Point(55, 44);
            this.buttonHalls.Name = "buttonHalls";
            this.buttonHalls.Size = new System.Drawing.Size(75, 23);
            this.buttonHalls.TabIndex = 0;
            this.buttonHalls.Text = "Зали";
            this.buttonHalls.UseVisualStyleBackColor = true;
            this.buttonHalls.Click += new System.EventHandler(this.buttonHalls_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 311);
            this.Controls.Add(this.groupBoxOperationInfo);
            this.Controls.Add(this.groupBoxGuide);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(295, 350);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Музей";
            this.groupBoxGuide.ResumeLayout(false);
            this.groupBoxOperationInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxGuide;
        private System.Windows.Forms.Button buttonEmployees;
        private System.Windows.Forms.GroupBox groupBoxOperationInfo;
        private System.Windows.Forms.Button buttonExcursions;
        private System.Windows.Forms.Button buttonExhibits;
        private System.Windows.Forms.Button buttonHalls;
    }
}

