﻿using System;
using System.Windows.Forms;

namespace SQLiteLab1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonEmployees_Click(object sender, EventArgs e)
        {
            var employees = new EmployeeForm();
            employees.ShowDialog();
        }

        private void buttonHalls_Click(object sender, EventArgs e)
        {
            var halls = new HallForm();
            halls.ShowDialog();
        }

        private void buttonExhibits_Click(object sender, EventArgs e)
        {
            var exhibits = new ExhibitForm();
            exhibits.ShowDialog();
        }

        private void buttonExcursion_Click(object sender, EventArgs e)
        {
            var excursions = new ExcursionForm();
            excursions.ShowDialog();
        }
    }
}