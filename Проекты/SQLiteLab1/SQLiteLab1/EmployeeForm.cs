﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SQLiteLab1
{
    public partial class EmployeeForm : Form
    {
        private readonly SqLiteDatabase _db;

        public EmployeeForm()
        {
            InitializeComponent();
            _db = new SqLiteDatabase();
        }

        private void EmployeeForm_Load(object sender, EventArgs e)
        {
            try
            {
                var query = "SELECT * FROM Employee;";
                var result = _db.GetDataTable(query);
                // выводим результат запроса
                dataGridViewResult.DataSource = result;
                result.Columns[0].ColumnName = "Номер працівника";
                result.Columns[1].ColumnName = "Ім'я";
                result.Columns[2].ColumnName = "Зарплата";
                result.Columns[3].ColumnName = "Професія";
                result.Columns[4].ColumnName = "Телефон";
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            _db.ClearRows("Employee");
            var data = new Dictionary<string, string>();
            try
            {
                for (var i = 0; i < dataGridViewResult.RowCount - 1; i++)
                {
                    data.Add("Id_employee", dataGridViewResult.Rows[i].Cells[0].Value.ToString());
                    data.Add("Name", dataGridViewResult.Rows[i].Cells[1].Value.ToString());
                    data.Add("Salary", dataGridViewResult.Rows[i].Cells[2].Value.ToString());
                    data.Add("Position", dataGridViewResult.Rows[i].Cells[3].Value.ToString());
                    data.Add("Telephone", dataGridViewResult.Rows[i].Cells[4].Value.ToString());
                    _db.Insert("Employee", data);
                    data.Clear();
                }
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
            }
        }
    }
}