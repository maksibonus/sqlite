﻿namespace SQLiteLab1
{
    partial class ExcursionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlExcursion = new System.Windows.Forms.TabControl();
            this.tabPageExcursion = new System.Windows.Forms.TabPage();
            this.labelExcursion = new System.Windows.Forms.Label();
            this.buttonSaveEmployees = new System.Windows.Forms.Button();
            this.dataGridViewEmployees = new System.Windows.Forms.DataGridView();
            this.comboBoxExcursion = new System.Windows.Forms.ComboBox();
            this.tabPageEmployee = new System.Windows.Forms.TabPage();
            this.labelEmployee = new System.Windows.Forms.Label();
            this.buttonSaveExcursions = new System.Windows.Forms.Button();
            this.dataGridViewExcursions = new System.Windows.Forms.DataGridView();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.tabControlExcursion.SuspendLayout();
            this.tabPageExcursion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmployees)).BeginInit();
            this.tabPageEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExcursions)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlExcursion
            // 
            this.tabControlExcursion.Controls.Add(this.tabPageExcursion);
            this.tabControlExcursion.Controls.Add(this.tabPageEmployee);
            this.tabControlExcursion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlExcursion.Location = new System.Drawing.Point(0, 0);
            this.tabControlExcursion.Name = "tabControlExcursion";
            this.tabControlExcursion.SelectedIndex = 0;
            this.tabControlExcursion.Size = new System.Drawing.Size(934, 296);
            this.tabControlExcursion.TabIndex = 0;
            this.tabControlExcursion.SelectedIndexChanged += new System.EventHandler(this.tabControlExcursion_SelectedIndexChanged);
            // 
            // tabPageExcursion
            // 
            this.tabPageExcursion.Controls.Add(this.labelExcursion);
            this.tabPageExcursion.Controls.Add(this.buttonSaveEmployees);
            this.tabPageExcursion.Controls.Add(this.dataGridViewEmployees);
            this.tabPageExcursion.Controls.Add(this.comboBoxExcursion);
            this.tabPageExcursion.Location = new System.Drawing.Point(4, 22);
            this.tabPageExcursion.Name = "tabPageExcursion";
            this.tabPageExcursion.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExcursion.Size = new System.Drawing.Size(926, 270);
            this.tabPageExcursion.TabIndex = 0;
            this.tabPageExcursion.Text = "Екскурсії";
            this.tabPageExcursion.UseVisualStyleBackColor = true;
            // 
            // labelExcursion
            // 
            this.labelExcursion.AutoSize = true;
            this.labelExcursion.Location = new System.Drawing.Point(563, 17);
            this.labelExcursion.Name = "labelExcursion";
            this.labelExcursion.Size = new System.Drawing.Size(57, 13);
            this.labelExcursion.TabIndex = 8;
            this.labelExcursion.Text = "Екскурсія";
            // 
            // buttonSaveEmployees
            // 
            this.buttonSaveEmployees.Location = new System.Drawing.Point(638, 50);
            this.buttonSaveEmployees.Name = "buttonSaveEmployees";
            this.buttonSaveEmployees.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveEmployees.TabIndex = 7;
            this.buttonSaveEmployees.Text = "Зберегти";
            this.buttonSaveEmployees.UseVisualStyleBackColor = true;
            this.buttonSaveEmployees.Click += new System.EventHandler(this.buttonSaveEmployees_Click);
            // 
            // dataGridViewEmployees
            // 
            this.dataGridViewEmployees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEmployees.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridViewEmployees.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewEmployees.Name = "dataGridViewEmployees";
            this.dataGridViewEmployees.Size = new System.Drawing.Size(554, 264);
            this.dataGridViewEmployees.TabIndex = 1;
            // 
            // comboBoxExcursion
            // 
            this.comboBoxExcursion.FormattingEnabled = true;
            this.comboBoxExcursion.Location = new System.Drawing.Point(638, 14);
            this.comboBoxExcursion.Name = "comboBoxExcursion";
            this.comboBoxExcursion.Size = new System.Drawing.Size(185, 21);
            this.comboBoxExcursion.TabIndex = 0;
            this.comboBoxExcursion.SelectedIndexChanged += new System.EventHandler(this.comboBoxExcursion_SelectedIndexChanged);
            // 
            // tabPageEmployee
            // 
            this.tabPageEmployee.Controls.Add(this.labelEmployee);
            this.tabPageEmployee.Controls.Add(this.buttonSaveExcursions);
            this.tabPageEmployee.Controls.Add(this.dataGridViewExcursions);
            this.tabPageEmployee.Controls.Add(this.comboBoxEmployee);
            this.tabPageEmployee.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmployee.Name = "tabPageEmployee";
            this.tabPageEmployee.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmployee.Size = new System.Drawing.Size(926, 270);
            this.tabPageEmployee.TabIndex = 1;
            this.tabPageEmployee.Text = "Працівники";
            this.tabPageEmployee.UseVisualStyleBackColor = true;
            // 
            // labelEmployee
            // 
            this.labelEmployee.AutoSize = true;
            this.labelEmployee.Location = new System.Drawing.Point(660, 18);
            this.labelEmployee.Name = "labelEmployee";
            this.labelEmployee.Size = new System.Drawing.Size(59, 13);
            this.labelEmployee.TabIndex = 9;
            this.labelEmployee.Text = "Працівник";
            // 
            // buttonSaveExcursions
            // 
            this.buttonSaveExcursions.Location = new System.Drawing.Point(737, 52);
            this.buttonSaveExcursions.Name = "buttonSaveExcursions";
            this.buttonSaveExcursions.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveExcursions.TabIndex = 7;
            this.buttonSaveExcursions.Text = "Зберегти";
            this.buttonSaveExcursions.UseVisualStyleBackColor = true;
            this.buttonSaveExcursions.Click += new System.EventHandler(this.buttonSaveExcursions_Click);
            // 
            // dataGridViewExcursions
            // 
            this.dataGridViewExcursions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewExcursions.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridViewExcursions.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewExcursions.Name = "dataGridViewExcursions";
            this.dataGridViewExcursions.Size = new System.Drawing.Size(651, 264);
            this.dataGridViewExcursions.TabIndex = 3;
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Location = new System.Drawing.Point(737, 15);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(180, 21);
            this.comboBoxEmployee.TabIndex = 2;
            this.comboBoxEmployee.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmployee_SelectedIndexChanged);
            // 
            // ExcursionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 296);
            this.Controls.Add(this.tabControlExcursion);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(950, 2500);
            this.MinimumSize = new System.Drawing.Size(950, 155);
            this.Name = "ExcursionForm";
            this.ShowIcon = false;
            this.Text = "Екскурсії працівників";
            this.Load += new System.EventHandler(this.ExcursionForm_Load);
            this.tabControlExcursion.ResumeLayout(false);
            this.tabPageExcursion.ResumeLayout(false);
            this.tabPageExcursion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmployees)).EndInit();
            this.tabPageEmployee.ResumeLayout(false);
            this.tabPageEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExcursions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlExcursion;
        private System.Windows.Forms.TabPage tabPageExcursion;
        private System.Windows.Forms.TabPage tabPageEmployee;
        private System.Windows.Forms.DataGridView dataGridViewEmployees;
        private System.Windows.Forms.ComboBox comboBoxExcursion;
        private System.Windows.Forms.DataGridView dataGridViewExcursions;
        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Button buttonSaveEmployees;
        private System.Windows.Forms.Button buttonSaveExcursions;
        private System.Windows.Forms.Label labelExcursion;
        private System.Windows.Forms.Label labelEmployee;
    }
}