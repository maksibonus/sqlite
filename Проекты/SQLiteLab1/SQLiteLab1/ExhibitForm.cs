﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SQLiteLab1
{
    public partial class ExhibitForm : Form
    {
        private readonly SqLiteDatabase _db;
        private bool _beforeLoad = true;

        public ExhibitForm()
        {
            InitializeComponent();
            _db = new SqLiteDatabase();
        }

        private void ExhibitForm_Load(object sender, EventArgs e)
        {
            try
            {
                var query = "SELECT Id_hall,Name FROM Hall;";
                var result = _db.GetDataTable(query);
                comboBoxHall.DataSource = result;
                comboBoxHall.DisplayMember = result.Columns[1].ColumnName;
                comboBoxHall.ValueMember = result.Columns[0].ColumnName;

                _beforeLoad = !_beforeLoad;
                //вызываем метод изменения индекса выбранного зала
                comboBoxHall_SelectedIndexChanged(comboBoxHall, new EventArgs());
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void comboBoxHall_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_beforeLoad)
            {
                return;
            }
            try
            {
                var query = "SELECT Id_exhibit,Name,Date_create,Author,Technique,Material FROM Exhibit WHERE Id_hall=" +
                            comboBoxHall.SelectedValue;
                var result = _db.GetDataTable(query);
                // выводим результат запроса
                dataGridViewExhibits.DataSource = result;
                // задаем имена столбцов
                result.Columns[0].ColumnName = "Номер експонату";
                result.Columns[1].ColumnName = "Назва";
                result.Columns[2].ColumnName = "Дата створення";
                result.Columns[3].ColumnName = "Автор";
                result.Columns[4].ColumnName = "Техніка";
                result.Columns[5].ColumnName = "Матеріал";
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            _db.ClearRowsWithCause("Exhibit", "Id_hall=" + comboBoxHall.SelectedValue);
            var data = new Dictionary<string, string>();
            try
            {
                for (var i = 0; i < dataGridViewExhibits.RowCount - 1; i++)
                {
                    data.Add("Name", dataGridViewExhibits.Rows[i].Cells[1].Value.ToString());
                    data.Add("Date_create", dataGridViewExhibits.Rows[i].Cells[2].Value.ToString());
                    data.Add("Author", dataGridViewExhibits.Rows[i].Cells[3].Value.ToString());
                    data.Add("Technique", dataGridViewExhibits.Rows[i].Cells[4].Value.ToString());
                    data.Add("Material", dataGridViewExhibits.Rows[i].Cells[5].Value.ToString());
                    data.Add("Id_hall", comboBoxHall.SelectedValue.ToString());
                    _db.Insert("Exhibit", data);
                    data.Clear();
                }
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
            }
        }
    }
}