﻿namespace SQLiteLab1
{
    partial class ExhibitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewExhibits = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxHall = new System.Windows.Forms.ComboBox();
            this.buttonSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExhibits)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewExhibits
            // 
            this.dataGridViewExhibits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewExhibits.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridViewExhibits.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewExhibits.Name = "dataGridViewExhibits";
            this.dataGridViewExhibits.Size = new System.Drawing.Size(656, 206);
            this.dataGridViewExhibits.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(662, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Зал";
            // 
            // comboBoxHall
            // 
            this.comboBoxHall.FormattingEnabled = true;
            this.comboBoxHall.Location = new System.Drawing.Point(694, 21);
            this.comboBoxHall.Name = "comboBoxHall";
            this.comboBoxHall.Size = new System.Drawing.Size(152, 21);
            this.comboBoxHall.TabIndex = 3;
            this.comboBoxHall.SelectedIndexChanged += new System.EventHandler(this.comboBoxHall_SelectedIndexChanged);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(694, 61);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "Зберегти";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // ExhibitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 206);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.dataGridViewExhibits);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxHall);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(875, 2500);
            this.MinimumSize = new System.Drawing.Size(875, 130);
            this.Name = "ExhibitForm";
            this.ShowIcon = false;
            this.Text = "Експонати";
            this.Load += new System.EventHandler(this.ExhibitForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewExhibits)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewExhibits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxHall;
        private System.Windows.Forms.Button buttonSave;
    }
}