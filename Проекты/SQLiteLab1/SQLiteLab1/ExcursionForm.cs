﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace SQLiteLab1
{
    public partial class ExcursionForm : Form
    {
        private readonly SqLiteDatabase _db;
        private bool _beforeLoad;
        private DataTable _excursionGrid, _employeeGrid;
        private bool _tabExcursion;

        public ExcursionForm()
        {
            InitializeComponent();
            _beforeLoad = true;
            _tabExcursion = true;
            _db = new SqLiteDatabase();
        }

        private void ExcursionForm_Load(object sender, EventArgs e)
        {
            try
            {
                // заполняем ComboBox для экскурсии
                var query = "SELECT Id_excursion,Name FROM Excursion;";
                var result = _db.GetDataTable(query);
                comboBoxExcursion.DataSource = result;
                comboBoxExcursion.DisplayMember = result.Columns[1].ColumnName;
                comboBoxExcursion.ValueMember = result.Columns[0].ColumnName;
                // заполняем ComboBox для работника
                query = "SELECT Id_employee,Name FROM Employee;";
                result = _db.GetDataTable(query);
                comboBoxEmployee.DataSource = result;
                comboBoxEmployee.DisplayMember = result.Columns[1].ColumnName;
                comboBoxEmployee.ValueMember = result.Columns[0].ColumnName;
                _beforeLoad = !_beforeLoad;

                // вызываем метод изменения индекса выбранной экскурсии
                comboBoxExcursion_SelectedIndexChanged(comboBoxExcursion, new EventArgs());
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void comboBoxExcursion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_beforeLoad)
            {
                return;
            }
            try
            {
                var query = @"SELECT DISTINCT EMP.Id_employee,EMP.Name,EMP.Salary,EMP.Position,EMP.Telephone 
                                FROM Employee EMP,Employee_Excursion EMP_EXC 
                                WHERE EMP_EXC.Id_excursion=" + comboBoxExcursion.SelectedValue +
                            " and EMP_EXC.Id_employee=EMP.Id_employee;";
                _employeeGrid = _db.GetDataTable(query);
                // выводим результат запроса
                dataGridViewEmployees.DataSource = _employeeGrid;
                // задаем имена столбцов
                _employeeGrid.Columns[0].ColumnName = "Номер працівника";
                _employeeGrid.Columns[1].ColumnName = "Ім'я";
                _employeeGrid.Columns[2].ColumnName = "Зарплата";
                _employeeGrid.Columns[3].ColumnName = "Професія";
                _employeeGrid.Columns[4].ColumnName = "Телефон";
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void comboBoxEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_beforeLoad)
            {
                return;
            }
            try
            {
                var query = @"SELECT DISTINCT EXC.Id_excursion, EXC.Name, EXC.Time, EXC.Date_expiration, EXC.Schedule, EXC.Cost 
                                FROM Excursion EXC,Employee_Excursion EMP_EXC 
                                WHERE EMP_EXC.Id_employee=" + comboBoxEmployee.SelectedValue +
                            " and EMP_EXC.Id_excursion=EXC.Id_excursion;";
                _excursionGrid = _db.GetDataTable(query);
                _excursionGrid.AcceptChanges();
                // выводим результат запроса
                dataGridViewExcursions.DataSource = _excursionGrid;
                // задаем имена столбцов
                _excursionGrid.Columns[0].ColumnName = "Номер екскурсії";
                _excursionGrid.Columns[1].ColumnName = "Назва";
                _excursionGrid.Columns[2].ColumnName = "Час початку";
                dataGridViewExcursions.Columns[2].DefaultCellStyle.Format = "HH:mm";
                _excursionGrid.Columns[3].ColumnName = "Дата закінчення";
                _excursionGrid.Columns[4].ColumnName = "Розклад";
                _excursionGrid.Columns[5].ColumnName = "Ціна";
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void buttonSaveEmployees_Click(object sender, EventArgs e)
        {
            try
            {
                string query;
                // удаляем строки из таблицы Employee_Excursion, у которых нет уже существующего рабочего
                var changesDeleteTable = _employeeGrid.GetChanges(DataRowState.Deleted);
                if (changesDeleteTable != null)
                {
                    for (var i = 0; i < changesDeleteTable.Rows.Count; i++)
                    {
                        query = @"delete from Employee_Excursion where Id_excursion="
                                + comboBoxExcursion.SelectedValue
                                + " and Id_employee="
                                + changesDeleteTable.Rows[i][0, DataRowVersion.Original] + ";";
                        _db.ExecuteNonQuery(query);
                    }
                }

                // добавляем в базу данных новые записи
                var changesAddTable = _employeeGrid.GetChanges(DataRowState.Added);
                if (changesAddTable != null)
                {
                    var dataAdd = new Dictionary<string, string>();
                    for (var i = 0; i < changesAddTable.Rows.Count; i++)
                    {
                        dataAdd.Add("Name", changesAddTable.Rows[i].ItemArray[1].ToString());
                        dataAdd.Add("Salary", changesAddTable.Rows[i].ItemArray[2].ToString());
                        dataAdd.Add("Position", changesAddTable.Rows[i].ItemArray[3].ToString());
                        dataAdd.Add("Telephone", changesAddTable.Rows[i].ItemArray[4].ToString());
                        _db.Insert("Employee", dataAdd);
                        dataAdd.Clear();
                        // получаем id добавленного рабочего
                        query = "select MAX(Id_employee) from Employee;";
                        var id = int.Parse(_db.ExecuteScalar(query));
                        // добавляем рабочего в участие в экскурсии
                        query = "insert into Employee_Excursion values(" + id + "," + comboBoxExcursion.SelectedValue +
                                ");";
                        _db.ExecuteNonQuery(query);
                    }
                }

                // обновляем записи в базе данных 
                var changesModTable = _employeeGrid.GetChanges(DataRowState.Modified);
                if (changesModTable == null) return;
                for (var i = 0; i < changesModTable.Rows.Count; i++)
                {
                    query = @"update Employee set 
                           Name='" + changesModTable.Rows[i].ItemArray[1] +
                            "',Salary=" + changesModTable.Rows[i].ItemArray[2] +
                            ",Position='" + changesModTable.Rows[i].ItemArray[3] +
                            "',Telephone='" + changesModTable.Rows[i].ItemArray[4] +
                            "' where Id_employee=" + changesModTable.Rows[i].ItemArray[0] + ";";
                    _db.ExecuteNonQuery(query);
                }
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void buttonSaveExcursions_Click(object sender, EventArgs e)
        {
            try
            {
                string query;
                // удаляем строки из таблицы Employee_Excursion, у которых нет уже существующей экскурсии
                var changesDeleteTable = _excursionGrid.GetChanges(DataRowState.Deleted);
                if (changesDeleteTable != null)
                {
                    for (var i = 0; i < changesDeleteTable.Rows.Count; i++)
                    {
                        query = @"delete from Employee_Excursion where Id_employee="
                                + comboBoxEmployee.SelectedValue
                                + " and Id_excursion="
                                + changesDeleteTable.Rows[i][0, DataRowVersion.Original] + ";";
                        _db.ExecuteNonQuery(query);
                    }
                }

                // добавляем в базу данных новые записи
                var changesAddTable = _excursionGrid.GetChanges(DataRowState.Added);
                if (changesAddTable != null)
                {
                    var dataAdd = new Dictionary<string, string>();
                    for (var i = 0; i < changesAddTable.Rows.Count; i++)
                    {
                        dataAdd.Add("Name", changesAddTable.Rows[i].ItemArray[1].ToString());
                        dataAdd.Add("Time",
                            DateTime.Parse(changesAddTable.Rows[i].ItemArray[2].ToString()).ToString("HH:mm"));
                        dataAdd.Add("Date_expiration",
                            DateTime.Parse(changesAddTable.Rows[i].ItemArray[3].ToString()).ToString("yyyy-MM-dd"));
                        dataAdd.Add("Schedule", changesAddTable.Rows[i].ItemArray[4].ToString());
                        dataAdd.Add("Cost", changesAddTable.Rows[i].ItemArray[5].ToString());
                        _db.Insert("Excursion", dataAdd);
                        dataAdd.Clear();
                        // получаем id добавленной экскурсии
                        query = "select MAX(Id_excursion) from Excursion;";
                        var id = int.Parse(_db.ExecuteScalar(query));
                        // добавляем к экскурсии рабочего
                        query = "insert into Employee_Excursion values(" + comboBoxEmployee.SelectedValue + "," + id +
                                ");";
                        _db.ExecuteNonQuery(query);
                    }
                }

                // обновляем записи в базе данных 
                var changesModTable = _excursionGrid.GetChanges(DataRowState.Modified);
                if (changesModTable == null) return;
                for (var i = 0; i < changesModTable.Rows.Count; i++)
                {
                    query = @"update Excursion set 
                           Name='" + changesModTable.Rows[i].ItemArray[1] +
                            "',Time='" +
                            DateTime.Parse(changesModTable.Rows[i].ItemArray[2].ToString()).ToString("HH:mm") +
                            "',Date_expiration='" +
                            DateTime.Parse(changesModTable.Rows[i].ItemArray[3].ToString()).ToString("yyyy-MM-dd") +
                            "',Schedule='" + changesModTable.Rows[i].ItemArray[4] +
                            "' ,Cost=" + changesModTable.Rows[i].ItemArray[5] +
                            " where Id_excursion=" + changesModTable.Rows[i].ItemArray[0] + ";";
                    _db.ExecuteNonQuery(query);
                }
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void tabControlExcursion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _tabExcursion = !_tabExcursion;
            if (_tabExcursion)
            {
                Text = @"Екскурсії працівників";
                comboBoxExcursion_SelectedIndexChanged(comboBoxExcursion, new EventArgs());
            }
            else
            {
                Text = @"Працівники екскурсій";
                comboBoxEmployee_SelectedIndexChanged(comboBoxEmployee, new EventArgs());
            }
        }
    }
}