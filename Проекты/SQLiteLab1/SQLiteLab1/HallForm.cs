﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SQLiteLab1
{
    public partial class HallForm : Form
    {
        private readonly SqLiteDatabase _db;
        private bool _beforeLoad = true;

        public HallForm()
        {
            InitializeComponent();
            _db = new SqLiteDatabase();
        }

        private void HallForm_Load(object sender, EventArgs e)
        {
            try
            {
                var query = "SELECT Id_employee, Name FROM Employee;";
                var result = _db.GetDataTable(query);
                comboBoxEmployee.DataSource = result;
                comboBoxEmployee.DisplayMember = result.Columns[1].ColumnName;
                comboBoxEmployee.ValueMember = result.Columns[0].ColumnName;
                _beforeLoad = !_beforeLoad;
                // вызываем метод изменения индекса выбранного работника
                comboBoxEmployee_SelectedIndexChanged(comboBoxEmployee, new EventArgs());
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            _db.ClearRowsWithCause("Hall", "Id_employee=" + comboBoxEmployee.SelectedValue);
            var data = new Dictionary<string, string>();
            try
            {
                for (var i = 0; i < dataGridViewHalls.RowCount - 1; i++)
                {
                    data.Add("Name", dataGridViewHalls.Rows[i].Cells[1].Value.ToString());
                    data.Add("Square", dataGridViewHalls.Rows[i].Cells[2].Value.ToString());
                    data.Add("Id_employee", comboBoxEmployee.SelectedValue.ToString());
                    _db.Insert("Hall", data);
                    data.Clear();
                }
            }
            catch (Exception fail)
            {
                MessageBox.Show(fail.Message);
            }
        }

        private void comboBoxEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_beforeLoad) return;
            try
            {
                var query = "SELECT Id_hall,Name,Square FROM Hall WHERE Id_employee=" +
                            comboBoxEmployee.SelectedValue + ";";
                var result = _db.GetDataTable(query);
                // выводим результат запроса
                dataGridViewHalls.DataSource = result;
                // задаем имена столбцов
                result.Columns[0].ColumnName = "Номер залу";
                result.Columns[1].ColumnName = "Назва";
                result.Columns[2].ColumnName = "Площа";
            }
            catch (Exception fail)
            {
                var error = "The following error has occurred:\n\n";
                error += fail.Message + "\n\n";
                MessageBox.Show(error);
                Close();
            }
        }
    }
}