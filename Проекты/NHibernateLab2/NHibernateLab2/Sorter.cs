﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace NHibernateLab2
{
    internal class Sorter : IComparer
    {
        public int Column = 0;
        public SortOrder Order = SortOrder.Ascending;
        public int Compare(object x, object y) // IComparer Member
        {
            if (!(x is ListViewItem))
                return (0);
            if (!(y is ListViewItem))
                return (0);
            var l1 = (ListViewItem) x;
            var l2 = (ListViewItem) y;
            if (l1.ListView.Columns[Column].Tag == null)
            {
                l1.ListView.Columns[Column].Tag = "Text";
            }
            if (l1.ListView.Columns[Column].Tag.ToString() == "Numeric")
            {
                var fl1 = float.Parse(l1.SubItems[Column].Text);
                var fl2 = float.Parse(l2.SubItems[Column].Text);

                return Order == SortOrder.Ascending ? fl1.CompareTo(fl2) : fl2.CompareTo(fl1);
            }
            var str1 = l1.SubItems[Column].Text;
            var str2 = l2.SubItems[Column].Text;
            if (Order == SortOrder.Ascending)
            {
                return string.Compare(str1, str2, StringComparison.Ordinal);
            }
            return string.Compare(str2, str1, StringComparison.Ordinal);
        }
    }
}