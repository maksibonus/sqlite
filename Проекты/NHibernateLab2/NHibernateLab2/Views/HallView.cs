﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;

namespace NHibernateLab2.Views
{
    public partial class HallView : Form, IHallView
    {
        private HallController _controller;
        public bool BeforeLoad;

        public HallView()
        {
            InitializeComponent();
            BeforeLoad = true;
            grdHalls.ListViewItemSorter = new Sorter();
            _controller = new HallController(this);
            _controller.LoadView();
        }

        #region Реализация IViewBase

        public void ClearGrid()
        {
            grdHalls.Columns.Clear();
            grdHalls.Columns.Add("Номер залу", 150, HorizontalAlignment.Right);
            grdHalls.Columns[0].Tag = "Numeric";
            grdHalls.Columns.Add("Назва", 150, HorizontalAlignment.Left);
            grdHalls.Columns.Add("Площа", 150, HorizontalAlignment.Right);
            grdHalls.Columns[2].Tag = "Numeric";
            // удаляем строки к grdHalls
            grdHalls.Items.Clear();
        }

        #endregion Реализация IViewBase

        private void FillComboBoxEmployee()
        {
            var dt = _controller.GetAllEmployeesIdAndName();
            comboBoxEmployee.DataSource = dt;
            comboBoxEmployee.DisplayMember = dt.Columns[1].ColumnName;
            comboBoxEmployee.ValueMember = dt.Columns[0].ColumnName;
        }

        #region Общие события формы

        private void HallView_Load(object sender, EventArgs e)
        {
            txtSquare.ValidatingType = typeof (float);
            FillComboBoxEmployee();
            comboBoxEmployee.SelectedIndex = 0;
            BeforeLoad = !BeforeLoad;
            // вызываем метод изменения индекса выбранной экскурсии
            comboBoxEmployee_SelectedIndexChanged(comboBoxEmployee, new EventArgs());
        }

        #endregion Общие события формы

        #region События формы регистрации

        private void txtSquare_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.IsValidInput) return;
            toolTipError.ToolTipTitle = "Некоректні дані";
            toolTipError.Show("Ви ввели некоректну площу. Будь ласка, змініть введені дані.", txtSquare, 5000);
            e.Cancel = true;
        }

        #endregion События формы регистрации

        #region События ListView

        private void grdHalls_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var s = (Sorter) grdHalls.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == SortOrder.Ascending)
            {
                s.Order = SortOrder.Descending;
            }
            else
            {
                s.Order = SortOrder.Ascending;
            }
            grdHalls.Sort();
        }

        #endregion События ListView

        #region События, которые передаются обратно в контроллер

        private void comboBoxEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BeforeLoad)
            {
                return;
            }
            _controller.LoadViewHalls(ValueEmployee);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _controller.AddNewHall(ValueEmployee);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            _controller.RemoveHall();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            _controller.Save();
        }

        private void grdHalls_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdHalls.SelectedItems.Count > 0)
                _controller.SelectedHallChanged(int.Parse(grdHalls.SelectedItems[0].Text));
        }

        #endregion События, которые передаются обратно в контроллер

        #region Реализация IView<T>

        public void AddEntityToGrid(Hall entity)
        {
            var parent = grdHalls.Items.Add(entity.Id_hall.ToString());
            parent.SubItems.Add(entity.Name);
            parent.SubItems.Add(entity.Square.ToString(CultureInfo.CurrentCulture));
        }

        public void UpdateGridWithChangedEntity(Hall entity)
        {
            var rowToUpdate = (from ListViewItem row in grdHalls.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_hall
                select row).FirstOrDefault();

            if (rowToUpdate == null) return;
            rowToUpdate.SubItems[1].Text = entity.Name;
            rowToUpdate.SubItems[2].Text = entity.Square.ToString(CultureInfo.CurrentCulture);
        }

        public void RemoveEntityFromGrid(Hall entity)
        {
            var rowToRemove = (from ListViewItem row in grdHalls.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_hall
                select row).FirstOrDefault();
            if (rowToRemove == null) return;
            grdHalls.Items.Remove(rowToRemove);
            grdHalls.Focus();
        }

        public string GetIdOfSelectedEntityInGrid()
        {
            if (grdHalls.SelectedItems.Count > 0)
                return grdHalls.SelectedItems[0].Text;
            return "";
        }

        public void SetSelectedEntityInGrid(Hall entity)
        {
            for (var index = 0; index < grdHalls.Items.Count; index++)
            {
                var row = grdHalls.Items[index];
                var selectedId = int.Parse(row.Text);
                if (selectedId == entity.Id_hall)
                {
                    row.Selected = true;
                }
            }
        }

        #endregion Реализация IView<T>

        #region Реализация IHallView

        public void SetController(HallController controller)
        {
            _controller = controller;
        }

        public string NAME
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public float SQUARE
        {
            get { return float.Parse(txtSquare.Text); }
            set { txtSquare.Text = value.ToString(CultureInfo.CurrentCulture); }
        }

        public int ValueEmployee
        {
            get { return int.Parse(comboBoxEmployee.SelectedValue.ToString()); }
            set { comboBoxEmployee.SelectedValue = value; }
        }

        public int IndexEmployee
        {
            get { return int.Parse(comboBoxEmployee.SelectedIndex.ToString()); }
            set { comboBoxEmployee.SelectedIndex = value; }
        }

        #endregion Реализация IHallView
    }
}