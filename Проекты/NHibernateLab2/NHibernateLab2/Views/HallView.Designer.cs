﻿namespace NHibernateLab2.Views
{
    partial class HallView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.grdHalls = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.txtSquare = new System.Windows.Forms.MaskedTextBox();
            this.lblSalary = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.toolTipError = new System.Windows.Forms.ToolTip(this.components);
            this.grpDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Location = new System.Drawing.Point(77, 18);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(214, 21);
            this.comboBoxEmployee.TabIndex = 0;
            this.comboBoxEmployee.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmployee_SelectedIndexChanged);
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(12, 21);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(59, 13);
            this.lblEmployee.TabIndex = 1;
            this.lblEmployee.Text = "Працівник";
            // 
            // grdHalls
            // 
            this.grdHalls.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.grdHalls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdHalls.FullRowSelect = true;
            this.grdHalls.GridLines = true;
            this.grdHalls.Location = new System.Drawing.Point(0, 166);
            this.grdHalls.Name = "grdHalls";
            this.grdHalls.Size = new System.Drawing.Size(459, 270);
            this.grdHalls.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdHalls.TabIndex = 51;
            this.grdHalls.UseCompatibleStateImageBehavior = false;
            this.grdHalls.View = System.Windows.Forms.View.Details;
            this.grdHalls.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.grdHalls_ColumnClick);
            this.grdHalls.SelectedIndexChanged += new System.EventHandler(this.grdHalls_SelectedIndexChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(336, 99);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(97, 23);
            this.btnRemove.TabIndex = 49;
            this.btnRemove.Text = "&Видалити";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(336, 70);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(97, 23);
            this.btnAdd.TabIndex = 48;
            this.btnAdd.Text = "&Додати";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(336, 130);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(97, 23);
            this.btnRegister.TabIndex = 50;
            this.btnRegister.Text = "&Оновити";
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // grpDetails
            // 
            this.grpDetails.Controls.Add(this.txtSquare);
            this.grpDetails.Controls.Add(this.lblSalary);
            this.grpDetails.Controls.Add(this.txtName);
            this.grpDetails.Controls.Add(this.lblName);
            this.grpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpDetails.Location = new System.Drawing.Point(9, 56);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Size = new System.Drawing.Size(310, 97);
            this.grpDetails.TabIndex = 47;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "Регістрація нового залу :";
            // 
            // txtSquare
            // 
            this.txtSquare.Location = new System.Drawing.Point(78, 54);
            this.txtSquare.Name = "txtSquare";
            this.txtSquare.Size = new System.Drawing.Size(219, 20);
            this.txtSquare.TabIndex = 40;
            this.txtSquare.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.txtSquare_TypeValidationCompleted);
            // 
            // lblSalary
            // 
            this.lblSalary.Location = new System.Drawing.Point(18, 57);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(63, 23);
            this.lblSalary.TabIndex = 23;
            this.lblSalary.Text = "Площа:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(78, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 20);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(18, 31);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(63, 23);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Назва:";
            // 
            // HallView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 436);
            this.Controls.Add(this.grdHalls);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.grpDetails);
            this.Controls.Add(this.lblEmployee);
            this.Controls.Add(this.comboBoxEmployee);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(475, 475);
            this.MinimumSize = new System.Drawing.Size(475, 475);
            this.Name = "HallView";
            this.ShowIcon = false;
            this.Text = "Зали";
            this.Load += new System.EventHandler(this.HallView_Load);
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Label lblEmployee;
        internal System.Windows.Forms.ListView grdHalls;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        internal System.Windows.Forms.Button btnRemove;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnRegister;
        internal System.Windows.Forms.GroupBox grpDetails;
        private System.Windows.Forms.MaskedTextBox txtSquare;
        internal System.Windows.Forms.Label lblSalary;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ToolTip toolTipError;
    }
}