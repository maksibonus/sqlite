﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;

namespace NHibernateLab2.Views
{
    public partial class ExcursionView : Form, IExcursionView, IEmployeeView
    {
        public bool _beforeLoad;
        private ExcursionController _controllerExc;
        private bool _tabExcursion;

        public ExcursionView()
        {
            InitializeComponent();
            _beforeLoad = true;
            grdEmployees.ListViewItemSorter = new Sorter();
            _controllerExc = new ExcursionController(this, this);
        }

        #region Реализация IViewBase

        public void ClearGrid()
        {
            grdEmployees.Columns.Clear();
            // добавляем столбцы к grdEmployees
            grdEmployees.Columns.Add("Номер працівника", 150, HorizontalAlignment.Right);
            grdEmployees.Columns[0].Tag = "Numeric";
            grdEmployees.Columns.Add("Ім'я", 150, HorizontalAlignment.Left);
            grdEmployees.Columns.Add("Зарплата", 150, HorizontalAlignment.Right);
            grdEmployees.Columns[2].Tag = "Numeric";
            grdEmployees.Columns.Add("Позиція", 150, HorizontalAlignment.Left);
            grdEmployees.Columns.Add("Телефон", 100, HorizontalAlignment.Left);

            grdEmployees.Items.Clear();
        }

        #endregion Реализация IViewBase

        private void FillComboBoxExcursion()
        {
            var dtExc = _controllerExc.GetAllExcursionsIdAndName();
            comboBoxExcursion.DataSource = dtExc;
            comboBoxExcursion.DisplayMember = dtExc.Columns[1].ColumnName;
            comboBoxExcursion.ValueMember = dtExc.Columns[0].ColumnName;
        }

        private void FillComboBoxEmployee()
        {
            var dtEmp = _controllerExc.GetAllEmployeesIdAndName();
            comboBoxEmployee.DataSource = dtEmp;
            comboBoxEmployee.DisplayMember = dtEmp.Columns[1].ColumnName;
            comboBoxEmployee.ValueMember = dtEmp.Columns[0].ColumnName;
        }

        #region Общие события формы

        private void ExcursionView_Load(object sender, EventArgs e)
        {
            txtSalary.ValidatingType = typeof (decimal);
            txtCost.ValidatingType = typeof (decimal);
            FillComboBoxExcursion();
            FillComboBoxEmployee();
            comboBoxExcursion.SelectedIndex = 0;
            comboBoxEmployee.SelectedIndex = 0;
            _beforeLoad = !_beforeLoad;
            // вызываем метод изменения индекса выбранной экскурсии
            comboBoxExcursion_SelectedIndexChanged(comboBoxExcursion, new EventArgs());
        }

        private void tabControlExcursion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _tabExcursion = !_tabExcursion;
            if (_tabExcursion)
            {
                Text = @"Екскурсії працівників";
                comboBoxExcursion_SelectedIndexChanged(comboBoxExcursion, new EventArgs());
            }
            else
            {
                Text = @"Працівники екскурсій";
                comboBoxEmployee_SelectedIndexChanged(comboBoxEmployee, new EventArgs());
            }
        }

        #endregion Общие события формы

        #region События ListView

        private void grdEmployees_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var s = (Sorter) grdEmployees.ListViewItemSorter;
            s.Column = e.Column;

            s.Order = s.Order == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            grdEmployees.Sort();
        }

        private void grdExcursions_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var s = (Sorter) grdExcursions.ListViewItemSorter;
            s.Column = e.Column;

            s.Order = s.Order == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            grdExcursions.Sort();
        }

        #endregion События ListView

        #region События формы регистрации

        private void txtCost_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.IsValidInput) return;
            toolTipError.ToolTipTitle = "Некоректні дані";
            toolTipError.Show("Ви ввели некоректну ціну. Будь ласка, змініть введені дані.", txtCost, 5000);
            e.Cancel = true;
        }

        private void txtSalary_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.IsValidInput) return;
            toolTipError.ToolTipTitle = "Некоректні дані";
            toolTipError.Show("Ви ввели некоректну зарплату. Будь ласка, змініть введені дані.", txtSalary, 5000);
            e.Cancel = true;
        }

        #endregion События формы регистрации

        #region События, которые передаются обратно в контроллер

        private void comboBoxExcursion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_beforeLoad)
            {
                return;
            }
            _controllerExc.LoadViewEmployees(ValueExcursion);
        }

        private void comboBoxEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_beforeLoad)
            {
                return;
            }
            _controllerExc.LoadViewExcursions(ValueEmployee);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _controllerExc.AddNewEmployeeWithExcursion(ValueExcursion);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            _controllerExc.RemoveEmployee();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            _controllerExc.Save();
        }

        private void grdEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdEmployees.SelectedItems.Count > 0)
                _controllerExc.SelectedEmployeeChanged(int.Parse(grdEmployees.SelectedItems[0].Text));
        }

        private void grdExcursions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdExcursions.SelectedItems.Count > 0)
                _controllerExc.SelectedExcursionChanged(int.Parse(grdExcursions.SelectedItems[0].Text));
        }

        private void buttonDeleteExc_Click(object sender, EventArgs e)
        {
            _controllerExc.RemoveExcursion();
        }

        private void buttonRegisterExc_Click(object sender, EventArgs e)
        {
            _controllerExc.SaveExcursion();
        }

        #endregion

        #region Реализация IView<Employee>

        public void AddEntityToGrid(Employee entity)
        {
            var parent = grdEmployees.Items.Add(entity.Id_employee.ToString());
            parent.SubItems.Add(entity.Name);
            parent.SubItems.Add(entity.Salary.ToString(CultureInfo.CurrentCulture));
            parent.SubItems.Add(entity.Position);
            parent.SubItems.Add(entity.Telephone);
        }

        public void UpdateGridWithChangedEntity(Employee entity)
        {
            var rowToUpdate = (from ListViewItem row in grdEmployees.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_employee
                select row).FirstOrDefault();

            if (rowToUpdate == null) return;
            rowToUpdate.SubItems[1].Text = entity.Name;
            rowToUpdate.SubItems[2].Text = entity.Salary.ToString(CultureInfo.CurrentCulture);
            rowToUpdate.SubItems[3].Text = entity.Position;
            rowToUpdate.SubItems[4].Text = entity.Telephone;
        }

        public void RemoveEntityFromGrid(Employee entity)
        {
            var rowToRemove = (from ListViewItem row in grdEmployees.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_employee
                select row).FirstOrDefault();
            if (rowToRemove == null) return;
            grdEmployees.Items.Remove(rowToRemove);
            grdEmployees.Focus();
        }

        string IView<Employee>.GetIdOfSelectedEntityInGrid()
        {
            if (grdEmployees.SelectedItems.Count > 0)
                return grdEmployees.SelectedItems[0].Text;
            return "";
        }

        public void SetSelectedEntityInGrid(Employee entity)
        {
            for (var index = 0; index < grdEmployees.Items.Count; index++)
            {
                var row = grdEmployees.Items[index];
                var selectedId = int.Parse(row.Text);
                if (selectedId == entity.Id_employee)
                {
                    row.Selected = true;
                }
            }
        }

        #endregion Реализация IView<Employee>

        #region Реализация IView<Excursion>

        public void AddEntityToGrid(Excursion entity)
        {
            var parent = grdExcursions.Items.Add(entity.Id_excursion.ToString());
            parent.SubItems.Add(entity.Name);
            parent.SubItems.Add(entity.Time.ToString("HH:mm"));
            parent.SubItems.Add(entity.Date_expiration.ToString("yyyy-MM-dd"));
            parent.SubItems.Add(entity.Schedule);
            parent.SubItems.Add(entity.Cost.ToString(CultureInfo.CurrentCulture));
        }

        public void UpdateGridWithChangedEntity(Excursion entity)
        {
            var rowToUpdate = (from ListViewItem row in grdExcursions.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_excursion
                select row).FirstOrDefault();

            if (rowToUpdate == null) return;
            rowToUpdate.SubItems[1].Text = entity.Name;
            rowToUpdate.SubItems[2].Text = entity.Time.ToString("HH:mm");
            rowToUpdate.SubItems[3].Text = entity.Date_expiration.ToString("yyyy-MM-dd");
            rowToUpdate.SubItems[4].Text = entity.Schedule;
            rowToUpdate.SubItems[5].Text = entity.Cost.ToString(CultureInfo.CurrentCulture);
        }

        public void RemoveEntityFromGrid(Excursion entity)
        {
            var rowToRemove = (from ListViewItem row in grdExcursions.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_excursion
                select row).FirstOrDefault();
            if (rowToRemove == null) return;
            grdExcursions.Items.Remove(rowToRemove);
            grdExcursions.Focus();
        }

        public void SetSelectedEntityInGrid(Excursion entity)
        {
            for (var index = 0; index < grdExcursions.Items.Count; index++)
            {
                var row = grdExcursions.Items[index];
                var selectedId = int.Parse(row.Text);
                if (selectedId == entity.Id_excursion)
                {
                    row.Selected = true;
                }
            }
        }

        string IView<Excursion>.GetIdOfSelectedEntityInGrid()
        {
            if (grdExcursions.SelectedItems.Count > 0)
                return grdExcursions.SelectedItems[0].Text;
            return "";
        }

        #endregion Реализация IView<Excursion>

        #region Реализация IEmployeeView

        public void SetController(EmployeeController controller)
        {
        }

        public void ClearGridEmployees()
        {
            ClearGrid();
        }

        string IEmployeeView.NAME
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public decimal SALARY
        {
            get { return decimal.Parse(txtSalary.Text); }
            set { txtSalary.Text = value.ToString(CultureInfo.CurrentCulture); }
        }

        public string POSITION
        {
            get { return txtPosition.Text; }
            set { txtPosition.Text = value; }
        }


        public string TELEPHONE
        {
            get { return txtTelephone.Text; }
            set { txtTelephone.Text = value; }
        }

        #endregion Реализация IEmployeeView

        #region Реализация IExcursionView

        string IExcursionView.NAME
        {
            get { return txtNameExcursion.Text; }
            set { txtNameExcursion.Text = value; }
        }

        public DateTime TIME
        {
            get { return dtpTime.Value; }
            set { dtpTime.Value = value; }
        }

        public DateTime DATE_EXPIRATION
        {
            get { return dtpDate.Value; }
            set { dtpDate.Value = value; }
        }

        public string SCHEDULE
        {
            get { return txtSchedule.Text; }
            set { txtSchedule.Text = value; }
        }

        public decimal COST
        {
            get { return decimal.Parse(txtCost.Text); }
            set { txtCost.Text = value.ToString(CultureInfo.CurrentCulture); }
        }

        public void SetController(ExcursionController controller)
        {
            _controllerExc = controller;
        }

        public void ClearGridExcursions()
        {
            grdExcursions.Columns.Clear();
            // добавляем столбцы к grdExcursions
            grdExcursions.Columns.Add("Номер екскурсії", 150, HorizontalAlignment.Right);
            grdExcursions.Columns[0].Tag = "Numeric";
            grdExcursions.Columns.Add("Назва", 150, HorizontalAlignment.Left);
            grdExcursions.Columns.Add("Час початку", 150, HorizontalAlignment.Right);
            grdExcursions.Columns.Add("Дата закінчення", 150, HorizontalAlignment.Right);
            grdExcursions.Columns.Add("Розклад", 100, HorizontalAlignment.Left);
            grdExcursions.Columns.Add("Ціна", 150, HorizontalAlignment.Right);
            grdExcursions.Columns[5].Tag = "Numeric";
            grdExcursions.Items.Clear();
        }

        public int ValueEmployee
        {
            get { return int.Parse(comboBoxEmployee.SelectedValue.ToString()); }
            set { comboBoxEmployee.SelectedValue = value; }
        }

        public int IndexEmployee
        {
            get { return int.Parse(comboBoxEmployee.SelectedIndex.ToString()); }
            set { comboBoxEmployee.SelectedIndex = value; }
        }

        public int ValueExcursion
        {
            get { return int.Parse(comboBoxExcursion.SelectedValue.ToString()); }
            set { comboBoxExcursion.SelectedValue = value; }
        }

        public int IndexExcursion
        {
            get { return int.Parse(comboBoxExcursion.SelectedIndex.ToString()); }
            set { comboBoxExcursion.SelectedIndex = value; }
        }

        #endregion Реализация IExcursionView
    }
}