﻿namespace NHibernateLab2.Views
{
    partial class ExhibitView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHall = new System.Windows.Forms.Label();
            this.comboBoxHall = new System.Windows.Forms.ComboBox();
            this.grdExhibits = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            this.txtTechnique = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lblDateCreate = new System.Windows.Forms.Label();
            this.lblTechnique = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.grpDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHall
            // 
            this.lblHall.AutoSize = true;
            this.lblHall.Location = new System.Drawing.Point(12, 19);
            this.lblHall.Name = "lblHall";
            this.lblHall.Size = new System.Drawing.Size(26, 13);
            this.lblHall.TabIndex = 4;
            this.lblHall.Text = "Зал";
            // 
            // comboBoxHall
            // 
            this.comboBoxHall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHall.FormattingEnabled = true;
            this.comboBoxHall.Location = new System.Drawing.Point(44, 16);
            this.comboBoxHall.Name = "comboBoxHall";
            this.comboBoxHall.Size = new System.Drawing.Size(194, 21);
            this.comboBoxHall.TabIndex = 3;
            this.comboBoxHall.SelectedIndexChanged += new System.EventHandler(this.comboBoxHall_SelectedIndexChanged);
            // 
            // grdExhibits
            // 
            this.grdExhibits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.grdExhibits.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdExhibits.FullRowSelect = true;
            this.grdExhibits.GridLines = true;
            this.grdExhibits.Location = new System.Drawing.Point(0, 170);
            this.grdExhibits.Name = "grdExhibits";
            this.grdExhibits.Size = new System.Drawing.Size(934, 266);
            this.grdExhibits.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdExhibits.TabIndex = 51;
            this.grdExhibits.UseCompatibleStateImageBehavior = false;
            this.grdExhibits.View = System.Windows.Forms.View.Details;
            this.grdExhibits.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.grdExhibits_ColumnClick);
            this.grdExhibits.SelectedIndexChanged += new System.EventHandler(this.grdExhibits_SelectedIndexChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(684, 101);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(97, 23);
            this.btnRemove.TabIndex = 49;
            this.btnRemove.Text = "&Видалити";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(684, 72);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(97, 23);
            this.btnAdd.TabIndex = 48;
            this.btnAdd.Text = "&Додати";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(684, 132);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(97, 23);
            this.btnRegister.TabIndex = 50;
            this.btnRegister.Text = "&Оновити";
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // grpDetails
            // 
            this.grpDetails.Controls.Add(this.txtMaterial);
            this.grpDetails.Controls.Add(this.txtTechnique);
            this.grpDetails.Controls.Add(this.lblMaterial);
            this.grpDetails.Controls.Add(this.txtAuthor);
            this.grpDetails.Controls.Add(this.lblAuthor);
            this.grpDetails.Controls.Add(this.txtDate);
            this.grpDetails.Controls.Add(this.lblDateCreate);
            this.grpDetails.Controls.Add(this.lblTechnique);
            this.grpDetails.Controls.Add(this.txtName);
            this.grpDetails.Controls.Add(this.lblName);
            this.grpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpDetails.Location = new System.Drawing.Point(11, 54);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Size = new System.Drawing.Size(667, 110);
            this.grpDetails.TabIndex = 47;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "Регістрація нового  експонату:";
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(429, 54);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(207, 20);
            this.txtMaterial.TabIndex = 30;
            // 
            // txtTechnique
            // 
            this.txtTechnique.Location = new System.Drawing.Point(124, 57);
            this.txtTechnique.Name = "txtTechnique";
            this.txtTechnique.Size = new System.Drawing.Size(219, 20);
            this.txtTechnique.TabIndex = 29;
            // 
            // lblMaterial
            // 
            this.lblMaterial.Location = new System.Drawing.Point(361, 54);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(62, 23);
            this.lblMaterial.TabIndex = 31;
            this.lblMaterial.Text = "Матеріал:";
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(429, 28);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(207, 20);
            this.txtAuthor.TabIndex = 27;
            // 
            // lblAuthor
            // 
            this.lblAuthor.Location = new System.Drawing.Point(361, 28);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(80, 23);
            this.lblAuthor.TabIndex = 28;
            this.lblAuthor.Text = "Автор:";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(124, 83);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(219, 20);
            this.txtDate.TabIndex = 5;
            // 
            // lblDateCreate
            // 
            this.lblDateCreate.Location = new System.Drawing.Point(18, 83);
            this.lblDateCreate.Name = "lblDateCreate";
            this.lblDateCreate.Size = new System.Drawing.Size(100, 23);
            this.lblDateCreate.TabIndex = 25;
            this.lblDateCreate.Text = "Дата створення:";
            // 
            // lblTechnique
            // 
            this.lblTechnique.Location = new System.Drawing.Point(18, 57);
            this.lblTechnique.Name = "lblTechnique";
            this.lblTechnique.Size = new System.Drawing.Size(63, 23);
            this.lblTechnique.TabIndex = 23;
            this.lblTechnique.Text = "Техніка:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(124, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 20);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(18, 31);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(63, 23);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Назва:";
            // 
            // ExhibitView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 436);
            this.Controls.Add(this.grdExhibits);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.grpDetails);
            this.Controls.Add(this.lblHall);
            this.Controls.Add(this.comboBoxHall);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(950, 475);
            this.MinimumSize = new System.Drawing.Size(950, 475);
            this.Name = "ExhibitView";
            this.ShowIcon = false;
            this.Text = "Експонати";
            this.Load += new System.EventHandler(this.ExhibitView_Load);
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHall;
        private System.Windows.Forms.ComboBox comboBoxHall;
        internal System.Windows.Forms.ListView grdExhibits;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        internal System.Windows.Forms.Button btnRemove;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnRegister;
        internal System.Windows.Forms.GroupBox grpDetails;
        internal System.Windows.Forms.TextBox txtAuthor;
        internal System.Windows.Forms.Label lblAuthor;
        internal System.Windows.Forms.TextBox txtDate;
        internal System.Windows.Forms.Label lblDateCreate;
        internal System.Windows.Forms.Label lblTechnique;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.Label lblName;
        internal System.Windows.Forms.TextBox txtTechnique;
        internal System.Windows.Forms.TextBox txtMaterial;
        internal System.Windows.Forms.Label lblMaterial;
    }
}