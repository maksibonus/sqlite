﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;

namespace NHibernateLab2.Views
{
    public partial class EmployeeView : Form, IEmployeeView
    {
        private EmployeeController _controller;

        public EmployeeView()
        {
            InitializeComponent();
            grdEmployees.ListViewItemSorter = new Sorter();
            _controller = new EmployeeController(this);
            _controller.LoadView();
        }

        #region Реализация IViewBase

        public void ClearGrid()
        {
            grdEmployees.Columns.Clear();
            grdEmployees.Columns.Add("Номер працівника", 150, HorizontalAlignment.Right);
            grdEmployees.Columns[0].Tag = "Numeric";
            grdEmployees.Columns.Add("Ім'я", 150, HorizontalAlignment.Left);
            grdEmployees.Columns.Add("Зарплата", 150, HorizontalAlignment.Right);
            grdEmployees.Columns[2].Tag = "Numeric";
            grdEmployees.Columns.Add("Позиція", 150, HorizontalAlignment.Left);
            grdEmployees.Columns.Add("Телефон", 100, HorizontalAlignment.Left);
            // удаляем строки к grdEmployees
            grdEmployees.Items.Clear();
        }

        #endregion Реализация IViewBase

        #region Общие события формы

        private void EmployeeView_Load(object sender, EventArgs e)
        {
            txtSalary.ValidatingType = typeof (decimal);
        }

        #endregion Общие события формы

        #region События формы регистрации

        private void txtSalary_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {
            if (e.IsValidInput) return;
            toolTipError.ToolTipTitle = "Некоректні дані";
            toolTipError.Show("Ви ввели некоректну зарплату. Будь ласка, змініть введені дані.", txtSalary, 5000);
            e.Cancel = true;
        }

        #endregion События формы регистрации

        #region События ListView

        private void grdEmployees_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var s = (Sorter) grdEmployees.ListViewItemSorter;
            s.Column = e.Column;

            if (s.Order == SortOrder.Ascending)
            {
                s.Order = SortOrder.Descending;
            }
            else
            {
                s.Order = SortOrder.Ascending;
            }
            grdEmployees.Sort();
        }

        #endregion События ListView

        #region События, которые передаются обратно в контроллер

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _controller.AddNewEmployee();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            _controller.RemoveEmployee();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            _controller.Save();
        }

        private void grdEmployees_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdEmployees.SelectedItems.Count > 0)
                _controller.SelectedEmployeeChanged(int.Parse(grdEmployees.SelectedItems[0].Text));
        }

        #endregion

        #region Реализация IView<T>

        public void AddEntityToGrid(Employee entity)
        {
            var parent = grdEmployees.Items.Add(entity.Id_employee.ToString());
            parent.SubItems.Add(entity.Name);
            parent.SubItems.Add(entity.Salary.ToString(CultureInfo.CurrentCulture));
            parent.SubItems.Add(entity.Position);
            parent.SubItems.Add(entity.Telephone);
        }

        public void UpdateGridWithChangedEntity(Employee entity)
        {
            var rowToUpdate = (from ListViewItem row in grdEmployees.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_employee
                select row).FirstOrDefault();

            if (rowToUpdate == null) return;
            rowToUpdate.SubItems[1].Text = entity.Name;
            rowToUpdate.SubItems[2].Text = entity.Salary.ToString(CultureInfo.CurrentCulture);
            rowToUpdate.SubItems[3].Text = entity.Position;
            rowToUpdate.SubItems[4].Text = entity.Telephone;
        }

        public void RemoveEntityFromGrid(Employee entity)
        {
            var rowToRemove = (from ListViewItem row in grdEmployees.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_employee
                select row).FirstOrDefault();
            if (rowToRemove == null) return;
            grdEmployees.Items.Remove(rowToRemove);
            grdEmployees.Focus();
        }

        string IView<Employee>.GetIdOfSelectedEntityInGrid()
        {
            if (grdEmployees.SelectedItems.Count > 0)
                return grdEmployees.SelectedItems[0].Text;
            return "";
        }

        public void SetSelectedEntityInGrid(Employee entity)
        {
            for (var index = 0; index < grdEmployees.Items.Count; index++)
            {
                var row = grdEmployees.Items[index];
                var selectedId = int.Parse(row.Text);
                if (selectedId == entity.Id_employee)
                {
                    row.Selected = true;
                }
            }
        }

        #endregion

        #region Реализация IEmployeeView

        public void SetController(EmployeeController controller)
        {
            _controller = controller;
        }

        public void ClearGridEmployees()
        {
            ClearGrid();
        }

        public string NAME
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public decimal SALARY
        {
            get { return decimal.Parse(txtSalary.Text); }
            set { txtSalary.Text = value.ToString(CultureInfo.CurrentCulture); }
        }

        public string POSITION
        {
            get { return txtPosition.Text; }
            set { txtPosition.Text = value; }
        }


        public string TELEPHONE
        {
            get { return txtTelephone.Text; }
            set { txtTelephone.Text = value; }
        }

        #endregion
    }
}