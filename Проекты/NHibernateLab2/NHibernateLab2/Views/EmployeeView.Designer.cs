﻿namespace NHibernateLab2.Views
{
    partial class EmployeeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.txtSalary = new System.Windows.Forms.MaskedTextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.lblTelephone = new System.Windows.Forms.Label();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.lblPosition = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.grdEmployees = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.toolTipError = new System.Windows.Forms.ToolTip(this.components);
            this.grpDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDetails
            // 
            this.grpDetails.Controls.Add(this.txtSalary);
            this.grpDetails.Controls.Add(this.txtTelephone);
            this.grpDetails.Controls.Add(this.lblTelephone);
            this.grpDetails.Controls.Add(this.txtPosition);
            this.grpDetails.Controls.Add(this.lblPosition);
            this.grpDetails.Controls.Add(this.lblSalary);
            this.grpDetails.Controls.Add(this.txtName);
            this.grpDetails.Controls.Add(this.lblName);
            this.grpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpDetails.Location = new System.Drawing.Point(12, 12);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Size = new System.Drawing.Size(614, 97);
            this.grpDetails.TabIndex = 35;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "Регістрація нового працівника :";
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(78, 54);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(219, 20);
            this.txtSalary.TabIndex = 40;
            this.txtSalary.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.txtSalary_TypeValidationCompleted);
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(389, 54);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(207, 20);
            this.txtTelephone.TabIndex = 27;
            // 
            // lblTelephone
            // 
            this.lblTelephone.Location = new System.Drawing.Point(317, 57);
            this.lblTelephone.Name = "lblTelephone";
            this.lblTelephone.Size = new System.Drawing.Size(80, 23);
            this.lblTelephone.TabIndex = 28;
            this.lblTelephone.Text = "Телефон:";
            // 
            // txtPosition
            // 
            this.txtPosition.Location = new System.Drawing.Point(389, 28);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Size = new System.Drawing.Size(207, 20);
            this.txtPosition.TabIndex = 5;
            // 
            // lblPosition
            // 
            this.lblPosition.Location = new System.Drawing.Point(317, 31);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(80, 23);
            this.lblPosition.TabIndex = 25;
            this.lblPosition.Text = "Позиція:";
            // 
            // lblSalary
            // 
            this.lblSalary.Location = new System.Drawing.Point(18, 57);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(63, 23);
            this.lblSalary.TabIndex = 23;
            this.lblSalary.Text = "Зарплата:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(78, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 20);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(18, 31);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(63, 23);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Ім\'я:";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(632, 57);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(97, 23);
            this.btnRemove.TabIndex = 37;
            this.btnRemove.Text = "&Видалити";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(632, 26);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(97, 23);
            this.btnAdd.TabIndex = 36;
            this.btnAdd.Text = "&Додати";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(632, 86);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(97, 23);
            this.btnRegister.TabIndex = 38;
            this.btnRegister.Text = "&Оновити";
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // grdEmployees
            // 
            this.grdEmployees.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.grdEmployees.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdEmployees.FullRowSelect = true;
            this.grdEmployees.GridLines = true;
            this.grdEmployees.Location = new System.Drawing.Point(0, 126);
            this.grdEmployees.Name = "grdEmployees";
            this.grdEmployees.Size = new System.Drawing.Size(749, 270);
            this.grdEmployees.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdEmployees.TabIndex = 39;
            this.grdEmployees.UseCompatibleStateImageBehavior = false;
            this.grdEmployees.View = System.Windows.Forms.View.Details;
            this.grdEmployees.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.grdEmployees_ColumnClick);
            this.grdEmployees.SelectedIndexChanged += new System.EventHandler(this.grdEmployees_SelectedIndexChanged);
            // 
            // EmployeeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 396);
            this.Controls.Add(this.grdEmployees);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.grpDetails);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(765, 435);
            this.MinimumSize = new System.Drawing.Size(765, 435);
            this.Name = "EmployeeView";
            this.ShowIcon = false;
            this.Text = "Працівники";
            this.Load += new System.EventHandler(this.EmployeeView_Load);
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox grpDetails;
        internal System.Windows.Forms.TextBox txtTelephone;
        internal System.Windows.Forms.Label lblTelephone;
        internal System.Windows.Forms.Label lblSalary;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.Label lblName;
        internal System.Windows.Forms.Button btnRemove;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnRegister;
        internal System.Windows.Forms.TextBox txtPosition;
        internal System.Windows.Forms.Label lblPosition;
        internal System.Windows.Forms.ListView grdEmployees;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.MaskedTextBox txtSalary;
        private System.Windows.Forms.ToolTip toolTipError;
    }
}

