﻿using System;
using System.Linq;
using System.Windows.Forms;
using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;

namespace NHibernateLab2.Views
{
    public partial class ExhibitView : Form, IExhibitView
    {
        private ExhibitController _controller;
        public bool BeforeLoad;

        public ExhibitView()
        {
            InitializeComponent();
            BeforeLoad = true;
            grdExhibits.ListViewItemSorter = new Sorter();
            _controller = new ExhibitController(this);
            _controller.LoadView();
        }

        #region Реализация IViewBase

        public void ClearGrid()
        {
            grdExhibits.Columns.Clear();
            grdExhibits.Columns.Add("Номер експонату", 150, HorizontalAlignment.Right);
            grdExhibits.Columns[0].Tag = "Numeric";
            grdExhibits.Columns.Add("Назва", 150, HorizontalAlignment.Left);
            grdExhibits.Columns.Add("Техніка", 150, HorizontalAlignment.Left);
            grdExhibits.Columns.Add("Дата створення", 150, HorizontalAlignment.Left);
            grdExhibits.Columns.Add("Автор", 150, HorizontalAlignment.Left);
            grdExhibits.Columns.Add("Матеріал", 150, HorizontalAlignment.Left);
            // удаляем строки из grdExhibits
            grdExhibits.Items.Clear();
        }

        #endregion Реализация IViewBase

        private void FillComboBoxHall()
        {
            var dt = _controller.GetAllHallsIdAndName();
            comboBoxHall.DataSource = dt;
            comboBoxHall.DisplayMember = dt.Columns[1].ColumnName;
            comboBoxHall.ValueMember = dt.Columns[0].ColumnName;
        }

        #region Общие события формы

        private void ExhibitView_Load(object sender, EventArgs e)
        {
            FillComboBoxHall();
            comboBoxHall.SelectedIndex = 0;
            BeforeLoad = !BeforeLoad;
            // вызываем метод изменения индекса выбранного зала
            comboBoxHall_SelectedIndexChanged(comboBoxHall, new EventArgs());
        }

        #endregion Общие события формы

        #region События ListView

        private void grdExhibits_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            var s = (Sorter) grdExhibits.ListViewItemSorter;
            s.Column = e.Column;

            s.Order = s.Order == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            grdExhibits.Sort();
        }

        #endregion События ListView

        #region События, которые передаются обратно в контроллер

        private void comboBoxHall_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BeforeLoad)
            {
                return;
            }
            _controller.LoadViewExhibits(ValueHall);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _controller.AddNewExhibit(ValueHall);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            _controller.RemoveExhibit();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            _controller.Save();
        }

        private void grdExhibits_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (grdExhibits.SelectedItems.Count > 0)
                _controller.SelectedExhibitChanged(int.Parse(grdExhibits.SelectedItems[0].Text));
        }

        #endregion События, которые передаются обратно в контроллер

        #region Реализация IView<T>

        public void AddEntityToGrid(Exhibit entity)
        {
            var parent = grdExhibits.Items.Add(entity.Id_exhibit.ToString());
            parent.SubItems.Add(entity.Name);
            parent.SubItems.Add(entity.Technique);
            parent.SubItems.Add(entity.Date_create);
            parent.SubItems.Add(entity.Author);
            parent.SubItems.Add(entity.Material);
        }

        public void UpdateGridWithChangedEntity(Exhibit entity)
        {
            var rowToUpdate = (from ListViewItem row in grdExhibits.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_exhibit
                select row).FirstOrDefault();

            if (rowToUpdate == null) return;
            rowToUpdate.SubItems[1].Text = entity.Name;
            rowToUpdate.SubItems[2].Text = entity.Technique;
            rowToUpdate.SubItems[3].Text = entity.Date_create;
            rowToUpdate.SubItems[4].Text = entity.Author;
            rowToUpdate.SubItems[5].Text = entity.Material;
        }

        public void RemoveEntityFromGrid(Exhibit entity)
        {
            var rowToRemove = (from ListViewItem row in grdExhibits.Items
                let updateId = int.Parse(row.Text)
                where updateId == entity.Id_exhibit
                select row).FirstOrDefault();
            if (rowToRemove == null) return;
            grdExhibits.Items.Remove(rowToRemove);
            grdExhibits.Focus();
        }

        public string GetIdOfSelectedEntityInGrid()
        {
            return grdExhibits.SelectedItems.Count > 0 ? grdExhibits.SelectedItems[0].Text : "";
        }

        public void SetSelectedEntityInGrid(Exhibit entity)
        {
            for (var index = 0; index < grdExhibits.Items.Count; index++)
            {
                var row = grdExhibits.Items[index];
                var selectedId = int.Parse(row.Text);
                if (selectedId == entity.Id_exhibit)
                {
                    row.Selected = true;
                }
            }
        }

        #endregion Реализация IView<T>

        #region Реализация IExhibitView

        public void SetController(ExhibitController controller)
        {
            _controller = controller;
        }

        public string NAME
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public string TECHNIQUE
        {
            get { return txtTechnique.Text; }
            set { txtTechnique.Text = value; }
        }

        public string DATE_CREATE
        {
            get { return txtDate.Text; }
            set { txtDate.Text = value; }
        }

        public string AUTHOR
        {
            get { return txtAuthor.Text; }
            set { txtAuthor.Text = value; }
        }

        public string MATERIAL
        {
            get { return txtMaterial.Text; }
            set { txtMaterial.Text = value; }
        }

        public int ValueHall
        {
            get { return int.Parse(comboBoxHall.SelectedValue.ToString()); }
            set { comboBoxHall.SelectedValue = value; }
        }

        public int IndexHall
        {
            get { return int.Parse(comboBoxHall.SelectedIndex.ToString()); }
            set { comboBoxHall.SelectedIndex = value; }
        }

        #endregion Реализация IExhibitView
    }
}