﻿namespace NHibernateLab2.Views
{
    partial class ExcursionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControlExcursion = new System.Windows.Forms.TabControl();
            this.tabPageExcursion = new System.Windows.Forms.TabPage();
            this.grdEmployees = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRegister = new System.Windows.Forms.Button();
            this.grpDetails = new System.Windows.Forms.GroupBox();
            this.txtSalary = new System.Windows.Forms.MaskedTextBox();
            this.txtTelephone = new System.Windows.Forms.TextBox();
            this.lblTelephone = new System.Windows.Forms.Label();
            this.txtPosition = new System.Windows.Forms.TextBox();
            this.lblPosition = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblExcursion = new System.Windows.Forms.Label();
            this.comboBoxExcursion = new System.Windows.Forms.ComboBox();
            this.tabPageEmployee = new System.Windows.Forms.TabPage();
            this.grdExcursions = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonDeleteExc = new System.Windows.Forms.Button();
            this.buttonRegisterExc = new System.Windows.Forms.Button();
            this.grpDetailsExc = new System.Windows.Forms.GroupBox();
            this.dtpTime = new System.Windows.Forms.DateTimePicker();
            this.lblCost = new System.Windows.Forms.Label();
            this.txtCost = new System.Windows.Forms.MaskedTextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtSchedule = new System.Windows.Forms.TextBox();
            this.lblSchedule = new System.Windows.Forms.Label();
            this.lblEnd = new System.Windows.Forms.Label();
            this.lblBegin = new System.Windows.Forms.Label();
            this.txtNameExcursion = new System.Windows.Forms.TextBox();
            this.lblNameExcursion = new System.Windows.Forms.Label();
            this.lblEmployee = new System.Windows.Forms.Label();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.toolTipError = new System.Windows.Forms.ToolTip(this.components);
            this.tabControlExcursion.SuspendLayout();
            this.tabPageExcursion.SuspendLayout();
            this.grpDetails.SuspendLayout();
            this.tabPageEmployee.SuspendLayout();
            this.grpDetailsExc.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlExcursion
            // 
            this.tabControlExcursion.Controls.Add(this.tabPageExcursion);
            this.tabControlExcursion.Controls.Add(this.tabPageEmployee);
            this.tabControlExcursion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlExcursion.Location = new System.Drawing.Point(0, 0);
            this.tabControlExcursion.Name = "tabControlExcursion";
            this.tabControlExcursion.SelectedIndex = 0;
            this.tabControlExcursion.Size = new System.Drawing.Size(934, 451);
            this.tabControlExcursion.TabIndex = 0;
            this.tabControlExcursion.SelectedIndexChanged += new System.EventHandler(this.tabControlExcursion_SelectedIndexChanged);
            // 
            // tabPageExcursion
            // 
            this.tabPageExcursion.Controls.Add(this.grdEmployees);
            this.tabPageExcursion.Controls.Add(this.btnRemove);
            this.tabPageExcursion.Controls.Add(this.btnAdd);
            this.tabPageExcursion.Controls.Add(this.btnRegister);
            this.tabPageExcursion.Controls.Add(this.grpDetails);
            this.tabPageExcursion.Controls.Add(this.lblExcursion);
            this.tabPageExcursion.Controls.Add(this.comboBoxExcursion);
            this.tabPageExcursion.Location = new System.Drawing.Point(4, 22);
            this.tabPageExcursion.Name = "tabPageExcursion";
            this.tabPageExcursion.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExcursion.Size = new System.Drawing.Size(926, 425);
            this.tabPageExcursion.TabIndex = 0;
            this.tabPageExcursion.Text = "Екскурсії";
            this.tabPageExcursion.UseVisualStyleBackColor = true;
            // 
            // grdEmployees
            // 
            this.grdEmployees.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.grdEmployees.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdEmployees.FullRowSelect = true;
            this.grdEmployees.GridLines = true;
            this.grdEmployees.Location = new System.Drawing.Point(3, 152);
            this.grdEmployees.Name = "grdEmployees";
            this.grdEmployees.Size = new System.Drawing.Size(920, 270);
            this.grdEmployees.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdEmployees.TabIndex = 44;
            this.grdEmployees.UseCompatibleStateImageBehavior = false;
            this.grdEmployees.View = System.Windows.Forms.View.Details;
            this.grdEmployees.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.grdEmployees_ColumnClick);
            this.grdEmployees.SelectedIndexChanged += new System.EventHandler(this.grdEmployees_SelectedIndexChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(623, 90);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(97, 23);
            this.btnRemove.TabIndex = 42;
            this.btnRemove.Text = "&Видалити";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(623, 61);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(97, 23);
            this.btnAdd.TabIndex = 41;
            this.btnAdd.Text = "&Додати";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(623, 121);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(97, 23);
            this.btnRegister.TabIndex = 43;
            this.btnRegister.Text = "&Оновити";
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // grpDetails
            // 
            this.grpDetails.Controls.Add(this.txtSalary);
            this.grpDetails.Controls.Add(this.txtTelephone);
            this.grpDetails.Controls.Add(this.lblTelephone);
            this.grpDetails.Controls.Add(this.txtPosition);
            this.grpDetails.Controls.Add(this.lblPosition);
            this.grpDetails.Controls.Add(this.lblSalary);
            this.grpDetails.Controls.Add(this.txtName);
            this.grpDetails.Controls.Add(this.lblName);
            this.grpDetails.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpDetails.Location = new System.Drawing.Point(3, 47);
            this.grpDetails.Name = "grpDetails";
            this.grpDetails.Size = new System.Drawing.Size(614, 97);
            this.grpDetails.TabIndex = 40;
            this.grpDetails.TabStop = false;
            this.grpDetails.Text = "Регістрація нового працівника :";
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(78, 54);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(219, 20);
            this.txtSalary.TabIndex = 40;
            this.txtSalary.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.txtSalary_TypeValidationCompleted);
            // 
            // txtTelephone
            // 
            this.txtTelephone.Location = new System.Drawing.Point(389, 54);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(207, 20);
            this.txtTelephone.TabIndex = 27;
            // 
            // lblTelephone
            // 
            this.lblTelephone.Location = new System.Drawing.Point(317, 57);
            this.lblTelephone.Name = "lblTelephone";
            this.lblTelephone.Size = new System.Drawing.Size(80, 23);
            this.lblTelephone.TabIndex = 28;
            this.lblTelephone.Text = "Телефон:";
            // 
            // txtPosition
            // 
            this.txtPosition.Location = new System.Drawing.Point(389, 28);
            this.txtPosition.Name = "txtPosition";
            this.txtPosition.Size = new System.Drawing.Size(207, 20);
            this.txtPosition.TabIndex = 5;
            // 
            // lblPosition
            // 
            this.lblPosition.Location = new System.Drawing.Point(317, 31);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(80, 23);
            this.lblPosition.TabIndex = 25;
            this.lblPosition.Text = "Позиція:";
            // 
            // lblSalary
            // 
            this.lblSalary.Location = new System.Drawing.Point(18, 57);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(63, 23);
            this.lblSalary.TabIndex = 23;
            this.lblSalary.Text = "Зарплата:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(78, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 20);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(18, 31);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(63, 23);
            this.lblName.TabIndex = 19;
            this.lblName.Text = "Ім\'я:";
            // 
            // lblExcursion
            // 
            this.lblExcursion.AutoSize = true;
            this.lblExcursion.Location = new System.Drawing.Point(8, 21);
            this.lblExcursion.Name = "lblExcursion";
            this.lblExcursion.Size = new System.Drawing.Size(57, 13);
            this.lblExcursion.TabIndex = 8;
            this.lblExcursion.Text = "Екскурсія";
            // 
            // comboBoxExcursion
            // 
            this.comboBoxExcursion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExcursion.FormattingEnabled = true;
            this.comboBoxExcursion.Location = new System.Drawing.Point(71, 18);
            this.comboBoxExcursion.Name = "comboBoxExcursion";
            this.comboBoxExcursion.Size = new System.Drawing.Size(219, 21);
            this.comboBoxExcursion.TabIndex = 0;
            this.comboBoxExcursion.SelectedIndexChanged += new System.EventHandler(this.comboBoxExcursion_SelectedIndexChanged);
            // 
            // tabPageEmployee
            // 
            this.tabPageEmployee.Controls.Add(this.grdExcursions);
            this.tabPageEmployee.Controls.Add(this.buttonDeleteExc);
            this.tabPageEmployee.Controls.Add(this.buttonRegisterExc);
            this.tabPageEmployee.Controls.Add(this.grpDetailsExc);
            this.tabPageEmployee.Controls.Add(this.lblEmployee);
            this.tabPageEmployee.Controls.Add(this.comboBoxEmployee);
            this.tabPageEmployee.Location = new System.Drawing.Point(4, 22);
            this.tabPageEmployee.Name = "tabPageEmployee";
            this.tabPageEmployee.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEmployee.Size = new System.Drawing.Size(926, 425);
            this.tabPageEmployee.TabIndex = 1;
            this.tabPageEmployee.Text = "Працівники";
            this.tabPageEmployee.UseVisualStyleBackColor = true;
            // 
            // grdExcursions
            // 
            this.grdExcursions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.grdExcursions.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grdExcursions.FullRowSelect = true;
            this.grdExcursions.GridLines = true;
            this.grdExcursions.Location = new System.Drawing.Point(3, 152);
            this.grdExcursions.Name = "grdExcursions";
            this.grdExcursions.Size = new System.Drawing.Size(920, 270);
            this.grdExcursions.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.grdExcursions.TabIndex = 49;
            this.grdExcursions.UseCompatibleStateImageBehavior = false;
            this.grdExcursions.View = System.Windows.Forms.View.Details;
            this.grdExcursions.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.grdExcursions_ColumnClick);
            this.grdExcursions.SelectedIndexChanged += new System.EventHandler(this.grdExcursions_SelectedIndexChanged);
            // 
            // buttonDeleteExc
            // 
            this.buttonDeleteExc.Location = new System.Drawing.Point(747, 90);
            this.buttonDeleteExc.Name = "buttonDeleteExc";
            this.buttonDeleteExc.Size = new System.Drawing.Size(97, 23);
            this.buttonDeleteExc.TabIndex = 47;
            this.buttonDeleteExc.Text = "&Видалити";
            this.buttonDeleteExc.Click += new System.EventHandler(this.buttonDeleteExc_Click);
            // 
            // buttonRegisterExc
            // 
            this.buttonRegisterExc.Location = new System.Drawing.Point(747, 121);
            this.buttonRegisterExc.Name = "buttonRegisterExc";
            this.buttonRegisterExc.Size = new System.Drawing.Size(97, 23);
            this.buttonRegisterExc.TabIndex = 48;
            this.buttonRegisterExc.Text = "&Оновити";
            this.buttonRegisterExc.Click += new System.EventHandler(this.buttonRegisterExc_Click);
            // 
            // grpDetailsExc
            // 
            this.grpDetailsExc.Controls.Add(this.dtpTime);
            this.grpDetailsExc.Controls.Add(this.lblCost);
            this.grpDetailsExc.Controls.Add(this.txtCost);
            this.grpDetailsExc.Controls.Add(this.dtpDate);
            this.grpDetailsExc.Controls.Add(this.txtSchedule);
            this.grpDetailsExc.Controls.Add(this.lblSchedule);
            this.grpDetailsExc.Controls.Add(this.lblEnd);
            this.grpDetailsExc.Controls.Add(this.lblBegin);
            this.grpDetailsExc.Controls.Add(this.txtNameExcursion);
            this.grpDetailsExc.Controls.Add(this.lblNameExcursion);
            this.grpDetailsExc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpDetailsExc.Location = new System.Drawing.Point(3, 47);
            this.grpDetailsExc.Name = "grpDetailsExc";
            this.grpDetailsExc.Size = new System.Drawing.Size(738, 97);
            this.grpDetailsExc.TabIndex = 45;
            this.grpDetailsExc.TabStop = false;
            this.grpDetailsExc.Text = "Регістрація нової екскурсії:";
            // 
            // dtpTime
            // 
            this.dtpTime.CustomFormat = "HH:mm";
            this.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTime.Location = new System.Drawing.Point(420, 54);
            this.dtpTime.Name = "dtpTime";
            this.dtpTime.Size = new System.Drawing.Size(143, 20);
            this.dtpTime.TabIndex = 50;
            // 
            // lblCost
            // 
            this.lblCost.Location = new System.Drawing.Point(569, 28);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(35, 23);
            this.lblCost.TabIndex = 42;
            this.lblCost.Text = "Ціна:";
            // 
            // txtCost
            // 
            this.txtCost.Location = new System.Drawing.Point(604, 28);
            this.txtCost.Name = "txtCost";
            this.txtCost.Size = new System.Drawing.Size(124, 20);
            this.txtCost.TabIndex = 42;
            this.txtCost.TypeValidationCompleted += new System.Windows.Forms.TypeValidationEventHandler(this.txtCost_TypeValidationCompleted);
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(420, 28);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(143, 20);
            this.dtpDate.TabIndex = 41;
            // 
            // txtSchedule
            // 
            this.txtSchedule.Location = new System.Drawing.Point(92, 57);
            this.txtSchedule.Name = "txtSchedule";
            this.txtSchedule.Size = new System.Drawing.Size(219, 20);
            this.txtSchedule.TabIndex = 27;
            // 
            // lblSchedule
            // 
            this.lblSchedule.Location = new System.Drawing.Point(18, 60);
            this.lblSchedule.Name = "lblSchedule";
            this.lblSchedule.Size = new System.Drawing.Size(80, 23);
            this.lblSchedule.TabIndex = 28;
            this.lblSchedule.Text = "Розклад:";
            // 
            // lblEnd
            // 
            this.lblEnd.Location = new System.Drawing.Point(317, 31);
            this.lblEnd.Name = "lblEnd";
            this.lblEnd.Size = new System.Drawing.Size(97, 23);
            this.lblEnd.TabIndex = 25;
            this.lblEnd.Text = "Дата закінчення:";
            // 
            // lblBegin
            // 
            this.lblBegin.Location = new System.Drawing.Point(317, 57);
            this.lblBegin.Name = "lblBegin";
            this.lblBegin.Size = new System.Drawing.Size(78, 23);
            this.lblBegin.TabIndex = 23;
            this.lblBegin.Text = "Час початку:";
            // 
            // txtNameExcursion
            // 
            this.txtNameExcursion.Location = new System.Drawing.Point(92, 28);
            this.txtNameExcursion.Name = "txtNameExcursion";
            this.txtNameExcursion.Size = new System.Drawing.Size(219, 20);
            this.txtNameExcursion.TabIndex = 1;
            // 
            // lblNameExcursion
            // 
            this.lblNameExcursion.Location = new System.Drawing.Point(18, 31);
            this.lblNameExcursion.Name = "lblNameExcursion";
            this.lblNameExcursion.Size = new System.Drawing.Size(63, 23);
            this.lblNameExcursion.TabIndex = 19;
            this.lblNameExcursion.Text = "Назва:";
            // 
            // lblEmployee
            // 
            this.lblEmployee.AutoSize = true;
            this.lblEmployee.Location = new System.Drawing.Point(16, 23);
            this.lblEmployee.Name = "lblEmployee";
            this.lblEmployee.Size = new System.Drawing.Size(59, 13);
            this.lblEmployee.TabIndex = 9;
            this.lblEmployee.Text = "Працівник";
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Location = new System.Drawing.Point(81, 20);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(219, 21);
            this.comboBoxEmployee.TabIndex = 2;
            this.comboBoxEmployee.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmployee_SelectedIndexChanged);
            // 
            // ExcursionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 451);
            this.Controls.Add(this.tabControlExcursion);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(950, 490);
            this.MinimumSize = new System.Drawing.Size(950, 490);
            this.Name = "ExcursionView";
            this.ShowIcon = false;
            this.Text = "Екскурсії працівників";
            this.Load += new System.EventHandler(this.ExcursionView_Load);
            this.tabControlExcursion.ResumeLayout(false);
            this.tabPageExcursion.ResumeLayout(false);
            this.tabPageExcursion.PerformLayout();
            this.grpDetails.ResumeLayout(false);
            this.grpDetails.PerformLayout();
            this.tabPageEmployee.ResumeLayout(false);
            this.tabPageEmployee.PerformLayout();
            this.grpDetailsExc.ResumeLayout(false);
            this.grpDetailsExc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlExcursion;
        private System.Windows.Forms.TabPage tabPageExcursion;
        private System.Windows.Forms.TabPage tabPageEmployee;
        private System.Windows.Forms.ComboBox comboBoxExcursion;
        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Label lblExcursion;
        private System.Windows.Forms.Label lblEmployee;
        internal System.Windows.Forms.ListView grdEmployees;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        internal System.Windows.Forms.Button btnRemove;
        internal System.Windows.Forms.Button btnAdd;
        internal System.Windows.Forms.Button btnRegister;
        internal System.Windows.Forms.GroupBox grpDetails;
        private System.Windows.Forms.MaskedTextBox txtSalary;
        internal System.Windows.Forms.TextBox txtTelephone;
        internal System.Windows.Forms.Label lblTelephone;
        internal System.Windows.Forms.TextBox txtPosition;
        internal System.Windows.Forms.Label lblPosition;
        internal System.Windows.Forms.Label lblSalary;
        internal System.Windows.Forms.TextBox txtName;
        internal System.Windows.Forms.Label lblName;
        internal System.Windows.Forms.ListView grdExcursions;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        internal System.Windows.Forms.Button buttonDeleteExc;
        internal System.Windows.Forms.Button buttonRegisterExc;
        internal System.Windows.Forms.GroupBox grpDetailsExc;
        internal System.Windows.Forms.TextBox txtSchedule;
        internal System.Windows.Forms.Label lblSchedule;
        internal System.Windows.Forms.Label lblEnd;
        internal System.Windows.Forms.Label lblBegin;
        internal System.Windows.Forms.TextBox txtNameExcursion;
        internal System.Windows.Forms.Label lblNameExcursion;
        internal System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.MaskedTextBox txtCost;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.DateTimePicker dtpTime;
        private System.Windows.Forms.ToolTip toolTipError;
    }
}