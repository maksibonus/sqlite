﻿using System;
using System.Windows.Forms;

namespace NHibernateLab2.Views
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void buttonEmployees_Click(object sender, EventArgs e)
        {
            var employees = new EmployeeView();
            employees.ShowDialog();
        }

        private void buttonHalls_Click(object sender, EventArgs e)
        {
            var halls = new HallView();
            halls.ShowDialog();
        }

        private void buttonExhibits_Click(object sender, EventArgs e)
        {
            var exhibits = new ExhibitView();
            exhibits.ShowDialog();
        }

        private void buttonExcursion_Click(object sender, EventArgs e)
        {
            var excursions = new ExcursionView();
            excursions.ShowDialog();
        }
    }
}