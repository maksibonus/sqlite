﻿using System.Collections;

namespace NHibernateLab2.Repositories
{
    public interface IRepository<T>
    {
        /// <summary>
        ///     Возращает сущность по id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>Сущность</returns>
        T Get(int id);

        /// <summary>
        ///     Возращает все сущности
        /// </summary>
        /// <returns>Сущность</returns>
        IList GetAll();

        /// <summary>
        ///     Сохраняет или обновляет в таблицу сущностью
        /// </summary>
        /// <param name="entity">Сущность</param>
        void SaveOrUpdate(T entity);

        /// <summary>
        ///     Удалить сущность из таблицы
        /// </summary>
        /// <param name="entityType">Тип сущности</param>
        /// <param name="index">Индекс в таблице</param>
        void Delete(string entityType, int index);
    }
}