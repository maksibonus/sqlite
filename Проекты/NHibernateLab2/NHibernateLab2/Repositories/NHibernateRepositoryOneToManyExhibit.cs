﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Repositories
{
    internal class NHibernateRepositoryOneToManyExhibit : NHibernateRepository<Exhibit>
    {
        public void Save(Exhibit entity, int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                entity.Hall = session.Get<Hall>(index);
                session.Save(entity);
                transaction.Commit();
            }
        }

        public IList<Exhibit> GetExhibitsByHallT(int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
            {
                var subQuery = session.QueryOver<Hall>().Where(p => p.Id_hall == index).List();
                var query = session.QueryOver<Exhibit>().Where(p => p.Hall == subQuery[0]).List();
                return query;
            }
        }

        public IList GetExhibitsByHall(int index)
        {
            return GetExhibitsByHallT(index).ToList();
        }

        public IList GetAllHalls()
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
                return session.CreateCriteria(typeof (Hall)).List<Hall>().ToList();
        }
    }
}