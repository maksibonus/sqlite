﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Repositories
{
    public class NHibernateRepositoryEmpExc : NHibernateRepository<Excursion>
    {
        public void Save(Employee entity, int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                entity.Excursions.Add(session.Get<Excursion>(index));
                session.Save(entity);
                transaction.Commit();
            }
        }

        public void Save(Excursion entity, int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                entity.Employees.Add(session.Get<Employee>(index));
                session.Save(entity);
                transaction.Commit();
            }
        }

        public IList GetEmployeesByExcursion(int index)
        {
            return GetEmployeesByExcursionT(index).ToList();
        }

        public IList<Employee> GetEmployeesByExcursionT(int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
            {
                var query =
                    session.CreateSQLQuery(@"SELECT DISTINCT EMP.Id_employee,EMP.Name,EMP.Salary,EMP.Position,EMP.Telephone 
                                                                FROM Employee EMP,Employee_Excursion EMP_EXC 
                                                                WHERE EMP_EXC.Id_excursion=" + index +
                                           " and EMP_EXC.Id_employee=EMP.Id_employee;")
                        .AddEntity(typeof (Employee))
                        .List<Employee>();
                return query;
            }
        }

        public IList GetExcursionsByEmployee(int index)
        {
            return GetExcursionsByEmployeeT(index).ToList();
        }

        public IList<Excursion> GetExcursionsByEmployeeT(int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
            {
                var query =
                    session.CreateSQLQuery(@"SELECT DISTINCT EXC.Id_excursion, EXC.Name, EXC.Time, EXC.Date_expiration, EXC.Schedule, EXC.Cost 
                                                    FROM Excursion EXC,Employee_Excursion EMP_EXC 
                                                    WHERE EMP_EXC.Id_employee=" + index +
                                           " and EMP_EXC.Id_excursion=EXC.Id_excursion;")
                        .AddEntity(typeof (Excursion))
                        .List<Excursion>();
                return query;
            }
        }

        public IList GetAllEmployees()
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
                return session.CreateCriteria(typeof (Employee)).List<Employee>().ToList();
        }

        public IList GetAllExcursions()
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
                return session.CreateCriteria(typeof (Excursion)).List<Excursion>().ToList();
        }
    }
}