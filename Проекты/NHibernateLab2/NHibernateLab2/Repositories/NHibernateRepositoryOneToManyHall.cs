﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Repositories
{
    public class NHibernateRepositoryOneToManyHall : NHibernateRepository<Hall>
    {
        public void Save(Hall entity, int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                entity.Employee = session.Get<Employee>(index);
                session.Save(entity);
                transaction.Commit();
            }
        }

        public IList<Hall> GetHallsByEmployeeT(int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
            {
                var subQuery = session.QueryOver<Employee>().Where(p => p.Id_employee == index).List();
                var query = session.QueryOver<Hall>().Where(p => p.Employee == subQuery[0]).List();
                return query;
            }
        }

        public IList GetHallsByEmployee(int index)
        {
            return GetHallsByEmployeeT(index).ToList();
        }

        public IList GetAllEmployees()
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
                return session.CreateCriteria(typeof (Employee)).List<Employee>().ToList();
        }
    }
}