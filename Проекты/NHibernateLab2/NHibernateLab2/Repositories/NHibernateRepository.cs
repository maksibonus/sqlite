﻿using System.Collections;
using System.Linq;

namespace NHibernateLab2.Repositories
{
    public class NHibernateRepository<T> : IRepository<T> where T : class
    {
        public T Get(int id)
        {
            using (var session = NHibernateHelper.OpenSession())
                return session.Get<T>(id);
        }

        public IList GetAll()
        {
            using (var session = NHibernateHelper.OpenSession())
            using (session.BeginTransaction())
                return session.CreateCriteria(typeof (T)).List<T>().ToList();
        }

        public void Delete(string entityType, int index)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                var hqlDelete = "delete " + entityType + " e where e.Id_" + entityType.ToLower() + " = :id";
                session.CreateQuery(hqlDelete)
                    .SetInt32("id", index)
                    .ExecuteUpdate();
                transaction.Commit();
            }
        }

        public void SaveOrUpdate(T entity)
        {
            using (var session = NHibernateHelper.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.SaveOrUpdate(entity);
                transaction.Commit();
            }
        }
    }
}