﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace NHibernateLab2
{
    public static class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;

        public static ISessionFactory SessionFactory
        {
            // создает фабрику сессий
            get
            {
                return _sessionFactory ??
                       (_sessionFactory = Fluently.Configure()
                           .Database(SQLiteConfiguration.Standard.UsingFile("Museum.sqlite"))
                           .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Program>())
                           .BuildSessionFactory());
            }
        }

        public static ISession OpenSession()
        {
            // открывает и возвращает сессию
            return SessionFactory.OpenSession();
        }
    }
}