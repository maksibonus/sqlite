﻿using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Interfaces
{
    public interface IHallView : IView<Hall>
    {
        string NAME { get; set; }
        float SQUARE { get; set; }
        int IndexEmployee { get; set; }
        int ValueEmployee { get; set; }
        void SetController(HallController controller);
    }
}