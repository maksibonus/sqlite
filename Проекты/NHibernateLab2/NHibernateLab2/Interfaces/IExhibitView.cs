﻿using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Interfaces
{
    public interface IExhibitView : IView<Exhibit>
    {
        string NAME { get; set; }
        string TECHNIQUE { get; set; }
        string DATE_CREATE { get; set; }
        string AUTHOR { get; set; }
        string MATERIAL { get; set; }
        int IndexHall { get; set; }
        int ValueHall { get; set; }
        void SetController(ExhibitController controller);
    }
}