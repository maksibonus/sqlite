﻿using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Interfaces
{
    public interface IEmployeeView : IView<Employee>
    {
        string NAME { get; set; }
        decimal SALARY { get; set; }
        string POSITION { get; set; }
        string TELEPHONE { get; set; }
        void SetController(EmployeeController controller);
        void ClearGridEmployees();
    }
}