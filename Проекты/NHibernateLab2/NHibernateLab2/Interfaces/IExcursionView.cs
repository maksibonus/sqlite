﻿using System;
using NHibernateLab2.Controllers;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Interfaces
{
    public interface IExcursionView : IView<Excursion>
    {
        string NAME { get; set; }
        DateTime TIME { get; set; }
        DateTime DATE_EXPIRATION { get; set; }
        string SCHEDULE { get; set; }
        decimal COST { get; set; }
        int IndexEmployee { get; set; }
        int ValueEmployee { get; set; }
        int IndexExcursion { get; set; }
        int ValueExcursion { get; set; }
        void SetController(ExcursionController controller);
        void ClearGridExcursions();
    }
}