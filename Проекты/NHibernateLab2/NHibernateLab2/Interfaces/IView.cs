﻿namespace NHibernateLab2.Interfaces
{
    public interface IView<in T> : IViewBase
    {
        void AddEntityToGrid(T entity);
        void UpdateGridWithChangedEntity(T entity);
        void RemoveEntityFromGrid(T entity);
        string GetIdOfSelectedEntityInGrid();
        void SetSelectedEntityInGrid(T entity);
    }
}