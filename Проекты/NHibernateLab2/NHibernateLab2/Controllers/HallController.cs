﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;
using NHibernateLab2.Repositories;

namespace NHibernateLab2.Controllers
{
    public class HallController
    {
        private readonly NHibernateRepositoryOneToManyHall _hallRepo;
        private readonly IHallView _view;
        private bool _add;
        private IList _halls;
        private Hall _selectedHall;

        public HallController(IHallView view)
        {
            _view = view;
            _add = false;
            _hallRepo = new NHibernateRepositoryOneToManyHall();
            _halls = LoadDataAboutHalls();
            view.SetController(this);
        }

        #region RemoveMethods

        public void RemoveHall()
        {
            int id;
            var success = int.TryParse(_view.GetIdOfSelectedEntityInGrid(), out id);
            if (!success) return;
            var hallToRemove = _halls.Cast<Hall>().FirstOrDefault(hall => hall.Id_hall == id);

            if (hallToRemove == null) return;
            var newSelectedIndex = _halls.IndexOf(hallToRemove);
            _halls.Remove(hallToRemove);
            _hallRepo.Delete(typeof (Hall).Name, id);
            _view.RemoveEntityFromGrid(hallToRemove);

            if (newSelectedIndex > -1 && newSelectedIndex < _halls.Count)
            {
                _view.SetSelectedEntityInGrid((Hall) _halls[newSelectedIndex]);
            }
        }

        #endregion RemoveMethods

        public bool FindHall(Hall needHall)
        {
            var hall = _halls.Cast<Hall>().FirstOrDefault(h => h.Name == needHall.Name
                                                               && Math.Abs(h.Square - needHall.Square) < float.Epsilon);
            return hall != null;
        }

        #region AddMethods

        public void AddNewHall(int index)
        {
            if (_add && !FindHall(_selectedHall))
            {
                Save();
                _hallRepo.Save(_selectedHall, index);
            }
            else
            {
                _selectedHall = new Hall();
            }
            UpdateViewDetailValues(_selectedHall);
            _add = !_add;
        }

        public void Save()
        {
            UpdateHallWithViewValues(_selectedHall);
            if (!_halls.Contains(_selectedHall))
            {
                // добавляем новый Hall
                _halls.Add(_selectedHall);
                _view.AddEntityToGrid(_selectedHall);
            }
            else
            {
                // обновляем существующий Hall
                _hallRepo.SaveOrUpdate(_selectedHall);
                _view.UpdateGridWithChangedEntity(_selectedHall);
            }
            _view.SetSelectedEntityInGrid(_selectedHall);
        }

        #endregion AddMethods

        #region UpdateMethods

        public void UpdateViewDetailValues(Hall hall)
        {
            _view.NAME = hall.Name;
            _view.SQUARE = hall.Square;
        }

        private void UpdateHallWithViewValues(Hall hall)
        {
            hall.Name = _view.NAME;
            hall.Square = _view.SQUARE;
        }

        public void SelectedHallChanged(int selectedhallId)
        {
            foreach (var hall in _halls.Cast<Hall>().Where(hall => hall.Id_hall == selectedhallId))
            {
                _selectedHall = hall;
                UpdateViewDetailValues(hall);
                _view.SetSelectedEntityInGrid(hall);
                break;
            }
        }

        #endregion UpdateMethods

        #region LoadMethods

        public void LoadViewHalls(int index)
        {
            _view.ClearGrid();
            _halls = _hallRepo.GetHallsByEmployee(index);
            if (_halls.Count == 0)
                return;
            foreach (Hall hall in _halls)
                _view.AddEntityToGrid(hall);
            _view.SetSelectedEntityInGrid((Hall) _halls[0]);
        }

        public void LoadView()
        {
            _view.ClearGrid();
            foreach (Hall hall in _halls)
                _view.AddEntityToGrid(hall);

            _view.SetSelectedEntityInGrid((Hall) _halls[0]);
        }

        public IList LoadDataAboutHalls(int index = -1)
        {
            if (index == -1)
                return LoadDataAboutHallsWithoutIndex();
            return _hallRepo.GetHallsByEmployee(index);
        }

        public DataTable GetAllEmployeesIdAndName()
        {
            var employees = _hallRepo.GetAllEmployees();
            var dt = employees.ToDataTable<Employee>();
            for (var i = dt.Columns.Count - 1; i > 1; i--)
            {
                dt.Columns.RemoveAt(i);
            }
            return dt;
        }

        public IList LoadDataAboutHallsWithoutIndex()
        {
            return _hallRepo.GetAll();
        }

        #endregion LoadMethods
    }
}