﻿using System.Collections;
using System.Data;
using System.Linq;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;
using NHibernateLab2.Repositories;

namespace NHibernateLab2.Controllers
{
    public class ExhibitController
    {
        private readonly NHibernateRepositoryOneToManyExhibit _exhibitRepo;
        private readonly IExhibitView _view;
        private bool _add;
        private IList _exhibits;
        private Exhibit _selectedExhibit;

        public ExhibitController(IExhibitView view)
        {
            _view = view;
            _add = false;
            _exhibitRepo = new NHibernateRepositoryOneToManyExhibit();
            _exhibits = LoadDataAboutExhibits();
            view.SetController(this);
        }

        #region RemoveMethods

        public void RemoveExhibit()
        {
            int id;
            var success = int.TryParse(_view.GetIdOfSelectedEntityInGrid(), out id);
            if (!success) return;
            var exhibitToRemove = _exhibits.Cast<Exhibit>().FirstOrDefault(exhibit => exhibit.Id_exhibit == id);

            if (exhibitToRemove == null) return;
            var newSelectedIndex = _exhibits.IndexOf(exhibitToRemove);
            _exhibits.Remove(exhibitToRemove);
            _exhibitRepo.Delete(typeof (Exhibit).Name, id);
            _view.RemoveEntityFromGrid(exhibitToRemove);

            if (newSelectedIndex > -1 && newSelectedIndex < _exhibits.Count)
            {
                _view.SetSelectedEntityInGrid((Exhibit) _exhibits[newSelectedIndex]);
            }
        }

        #endregion RemoveMethods

        public bool FindExhibit(Exhibit needExhibit)
        {
            var exhibit = _exhibits.Cast<Exhibit>().FirstOrDefault(h => h.Name == needExhibit.Name
                                                                        && h.Technique == needExhibit.Technique
                                                                        && h.Date_create == needExhibit.Date_create
                                                                        && h.Author == needExhibit.Author
                                                                        && h.Material == needExhibit.Material);
            return exhibit != null;
        }

        #region AddMethods

        public void AddNewExhibit(int index)
        {
            if (_add && !FindExhibit(_selectedExhibit))
            {
                Save();
                _exhibitRepo.Save(_selectedExhibit, index);
            }
            else
            {
                _selectedExhibit = new Exhibit();
            }
            UpdateViewDetailValues(_selectedExhibit);
            _add = !_add;
        }

        public void Save()
        {
            UpdateExhibitWithViewValues(_selectedExhibit);
            if (!_exhibits.Contains(_selectedExhibit))
            {
                // добавляем новый Exhibit
                _exhibits.Add(_selectedExhibit);
                _view.AddEntityToGrid(_selectedExhibit);
            }
            else
            {
                // обновляем существующий Exhibit
                _exhibitRepo.SaveOrUpdate(_selectedExhibit);
                _view.UpdateGridWithChangedEntity(_selectedExhibit);
            }
            _view.SetSelectedEntityInGrid(_selectedExhibit);
        }

        #endregion AddMethods

        #region UpdateMethods

        public void UpdateViewDetailValues(Exhibit exhibit)
        {
            _view.NAME = exhibit.Name;
            _view.TECHNIQUE = exhibit.Technique;
            _view.DATE_CREATE = exhibit.Date_create;
            _view.AUTHOR = exhibit.Author;
            _view.MATERIAL = exhibit.Material;
        }

        private void UpdateExhibitWithViewValues(Exhibit exhibit)
        {
            exhibit.Name = _view.NAME;
            exhibit.Technique = _view.TECHNIQUE;
            exhibit.Date_create = _view.DATE_CREATE;
            exhibit.Author = _view.AUTHOR;
            exhibit.Material = _view.MATERIAL;
        }

        public void SelectedExhibitChanged(int selectedExhibitId)
        {
            foreach (var exhibit in _exhibits.Cast<Exhibit>().Where(exhibit => exhibit.Id_exhibit == selectedExhibitId))
            {
                _selectedExhibit = exhibit;
                UpdateViewDetailValues(exhibit);
                _view.SetSelectedEntityInGrid(exhibit);
                break;
            }
        }

        #endregion UpdateMethods

        #region LoadMethods

        public void LoadViewExhibits(int index)
        {
            _view.ClearGrid();
            _exhibits = _exhibitRepo.GetExhibitsByHall(index);
            if (_exhibits.Count == 0)
                return;
            foreach (Exhibit exhibit in _exhibits)
                _view.AddEntityToGrid(exhibit);
            _view.SetSelectedEntityInGrid((Exhibit) _exhibits[0]);
        }

        public void LoadView()
        {
            _view.ClearGrid();
            foreach (Exhibit exhibit in _exhibits)
                _view.AddEntityToGrid(exhibit);

            _view.SetSelectedEntityInGrid((Exhibit) _exhibits[0]);
        }

        public IList LoadDataAboutExhibits(int index = -1)
        {
            if (index == -1)
                return LoadDataAboutExhibitsWithoutIndex();
            return _exhibitRepo.GetExhibitsByHall(index);
        }

        public DataTable GetAllHallsIdAndName()
        {
            var halls = _exhibitRepo.GetAllHalls();
            var dt = halls.ToDataTable<Hall>();
            for (var i = dt.Columns.Count - 1; i > 1; i--)
            {
                dt.Columns.RemoveAt(i);
            }
            return dt;
        }

        public IList LoadDataAboutExhibitsWithoutIndex()
        {
            return _exhibitRepo.GetAll();
        }

        #endregion LoadMethods
    }
}