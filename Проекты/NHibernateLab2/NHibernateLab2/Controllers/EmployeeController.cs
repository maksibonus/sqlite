﻿using System.Collections;
using System.Linq;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;
using NHibernateLab2.Repositories;

namespace NHibernateLab2.Controllers
{
    public class EmployeeController
    {
        private readonly IRepository<Employee> _employeeRepo;
        protected readonly IEmployeeView View;
        protected bool Add;
        protected IList Employees;
        protected Employee SelectedEmployee;

        public EmployeeController(IEmployeeView view)
        {
            View = view;
            Add = false;
            _employeeRepo = new NHibernateRepository<Employee>();
            Employees = LoadData();
            view.SetController(this);
        }

        #region RemoveMethods

        public void RemoveEmployee()
        {
            int id;
            var success = int.TryParse(View.GetIdOfSelectedEntityInGrid(), out id);
            if (!success) return;
            var employeeToRemove = Employees.Cast<Employee>().FirstOrDefault(emp => emp.Id_employee == id);

            if (employeeToRemove == null) return;
            var newSelectedIndex = Employees.IndexOf(employeeToRemove);
            Employees.Remove(employeeToRemove);
            _employeeRepo.Delete(typeof (Employee).Name, id);
            View.RemoveEntityFromGrid(employeeToRemove);

            if (newSelectedIndex > -1 && newSelectedIndex < Employees.Count)
            {
                View.SetSelectedEntityInGrid((Employee) Employees[newSelectedIndex]);
            }
        }

        #endregion RemoveMethods

        public bool FindEmployee(Employee needEmployee)
        {
            var employee = Employees.Cast<Employee>().FirstOrDefault(emp => emp.Name == needEmployee.Name
                                                                             && emp.Position == needEmployee.Position
                                                                             && emp.Salary == needEmployee.Salary
                                                                             && emp.Position == needEmployee.Position);
            return employee != null;
        }

        #region AddMethods

        public void AddNewEmployee()
        {
            if (Add && !FindEmployee(SelectedEmployee))
            {
                Save();
                _employeeRepo.SaveOrUpdate(SelectedEmployee);
            }
            else
            {
                SelectedEmployee = new Employee();
            }
            UpdateViewDetailValues(SelectedEmployee);
            Add = !Add;
        }

        public void Save()
        {
            UpdateEmployeeWithViewValues(SelectedEmployee);
            if (!Employees.Contains(SelectedEmployee))
            {
                // добавляем нового Employee
                Employees.Add(SelectedEmployee);
                View.AddEntityToGrid(SelectedEmployee);
            }
            else
            {
                // обновляем существующего Employee
                _employeeRepo.SaveOrUpdate(SelectedEmployee);
                View.UpdateGridWithChangedEntity(SelectedEmployee);
            }
            View.SetSelectedEntityInGrid(SelectedEmployee);
        }

        #endregion AddMethods

        #region UpdateMethods

        public void UpdateViewDetailValues(Employee emp)
        {
            View.NAME = emp.Name;
            View.SALARY = emp.Salary;
            View.POSITION = emp.Position;
            View.TELEPHONE = emp.Telephone;
        }

        private void UpdateEmployeeWithViewValues(Employee emp)
        {
            emp.Name = View.NAME;
            emp.Salary = View.SALARY;
            emp.Position = View.POSITION;
            emp.Telephone = View.TELEPHONE;
        }

        public void SelectedEmployeeChanged(int selectedEmployeeId)
        {
            foreach (var emp in Employees.Cast<Employee>().Where(emp => emp.Id_employee == selectedEmployeeId))
            {
                SelectedEmployee = emp;
                UpdateViewDetailValues(emp);
                View.SetSelectedEntityInGrid(emp);
                break;
            }
        }

        #endregion UpdateMethods

        #region LoadMethods

        private IList LoadData()
        {
            var list = _employeeRepo.GetAll();
            return list;
        }

        public void LoadView()
        {
            View.ClearGrid();
            foreach (Employee emp in Employees)
                View.AddEntityToGrid(emp);

            View.SetSelectedEntityInGrid((Employee) Employees[0]);
        }

        #endregion LoadMethods
    }
}