﻿using System.Collections;
using System.Data;
using System.Linq;
using NHibernateLab2.Entities;
using NHibernateLab2.Interfaces;
using NHibernateLab2.Repositories;

namespace NHibernateLab2.Controllers
{
    public class ExcursionController : EmployeeController
    {
        private readonly NHibernateRepositoryEmpExc _excursionRepo;
        private readonly IExcursionView _viewExc;
        private IList _excursions;
        private Excursion _selectedExcursion;

        public ExcursionController(IExcursionView viewExc, IEmployeeView viewEmp)
            : base(viewEmp)
        {
            _viewExc = viewExc;
            _viewExc.SetController(this);
            _excursionRepo = new NHibernateRepositoryEmpExc();
            Employees = LoadDataAboutEmployees(-1);
            _excursions = LoadDataAboutExcursions(-1);
        }

        #region RemoveMethods

        public void RemoveExcursion()
        {
            int id;
            var success = int.TryParse(((IExcursionView) View).GetIdOfSelectedEntityInGrid(), out id);
            if (!success) return;
            var excursionToRemove = _excursions.Cast<Excursion>().FirstOrDefault(exc => exc.Id_excursion == id);

            if (excursionToRemove == null) return;
            var newSelectedIndex = _excursions.IndexOf(excursionToRemove);
            _excursions.Remove(excursionToRemove);
            _excursionRepo.Delete(typeof (Excursion).Name, id);
            _viewExc.RemoveEntityFromGrid(excursionToRemove);

            if (newSelectedIndex > -1 && newSelectedIndex < _excursions.Count)
            {
                _viewExc.SetSelectedEntityInGrid((Excursion) _excursions[newSelectedIndex]);
            }
        }

        #endregion RemoveMethods

        #region AddMethods

        public void AddNewEmployeeWithExcursion(int index)
        {
            if (Add && !FindEmployee(SelectedEmployee))
            {
                Save();
                _excursionRepo.Save(SelectedEmployee, index);
            }
            else
            {
                SelectedEmployee = new Employee();
            }
            UpdateViewDetailValues(SelectedEmployee);
            Add = !Add;
        }

        public void SaveExcursion()
        {
            UpdateExcursionWithViewValues(_selectedExcursion);
            if (!_excursions.Contains(_selectedExcursion))
            {
                // добавляем новую Excursion
                _excursions.Add(_selectedExcursion);
                _viewExc.AddEntityToGrid(_selectedExcursion);
            }
            else
            {
                // обновляем существующую Excursion
                _excursionRepo.SaveOrUpdate(_selectedExcursion);
                _viewExc.UpdateGridWithChangedEntity(_selectedExcursion);
            }
            _viewExc.SetSelectedEntityInGrid(_selectedExcursion);
        }

        #endregion AddMethods

        #region UpdateMethods

        private void UpdateViewExcDetailValues(Excursion exc)
        {
            _viewExc.NAME = exc.Name;
            _viewExc.TIME = exc.Time;
            _viewExc.DATE_EXPIRATION = exc.Date_expiration;
            _viewExc.SCHEDULE = exc.Schedule;
            _viewExc.COST = exc.Cost;
        }

        private void UpdateExcursionWithViewValues(Excursion exc)
        {
            exc.Name = _viewExc.NAME;
            exc.Time = _viewExc.TIME;
            exc.Date_expiration = _viewExc.DATE_EXPIRATION;
            exc.Schedule = _viewExc.SCHEDULE;
            exc.Cost = _viewExc.COST;
        }

        public void SelectedExcursionChanged(int selectedExcursionId)
        {
            foreach (var exc in _excursions.Cast<Excursion>().Where(exc => exc.Id_excursion == selectedExcursionId))
            {
                _selectedExcursion = exc;
                UpdateViewExcDetailValues(exc);
                _viewExc.SetSelectedEntityInGrid(exc);
                break;
            }
        }

        #endregion UpdateMethods

        #region LoadMethods

        public void LoadViewEmployees(int index)
        {
            View.ClearGridEmployees();
            Employees = _excursionRepo.GetEmployeesByExcursion(index);
            if (Employees.Count == 0)
                return;
            foreach (Employee emp in Employees)
                View.AddEntityToGrid(emp);
            View.SetSelectedEntityInGrid((Employee) Employees[0]);
        }

        public void LoadViewExcursions(int index)
        {
            _viewExc.ClearGridExcursions();
            _excursions = _excursionRepo.GetExcursionsByEmployee(index);
            if (_excursions.Count == 0)
                return;
            foreach (Excursion exc in _excursions)
                _viewExc.AddEntityToGrid(exc);
            _viewExc.SetSelectedEntityInGrid((Excursion) _excursions[0]);
        }

        public DataTable GetAllExcursionsIdAndName()
        {
            var dt = _excursions.ToDataTable<Excursion>();
            for (var i = dt.Columns.Count - 1; i > 1; i--)
            {
                dt.Columns.RemoveAt(i);
            }
            return dt;
        }

        public DataTable GetAllEmployeesIdAndName()
        {
            var dt = Employees.ToDataTable<Employee>();
            for (var i = dt.Columns.Count - 1; i > 1; i--)
            {
                dt.Columns.RemoveAt(i);
            }
            return dt;
        }

        public IList LoadDataAboutEmployees(int index)
        {
            if (index == -1)
                return LoadDataAboutEmployeesWithoutIndex();
            return _excursionRepo.GetEmployeesByExcursion(index);
        }

        public IList LoadDataAboutEmployeesWithoutIndex()
        {
            return _excursionRepo.GetAllEmployees();
        }

        public IList LoadDataAboutExcursions(int index)
        {
            if (index == -1)
                return LoadDataAboutExcursionsWithoutIndex();
            return _excursionRepo.GetExcursionsByEmployee(index);
        }

        public IList LoadDataAboutExcursionsWithoutIndex()
        {
            return _excursionRepo.GetAllExcursions();
        }

        #endregion LoadMethods
    }
}