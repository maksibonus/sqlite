﻿using FluentNHibernate.Mapping;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Mappings
{
    internal class ExcursionMap : ClassMap<Excursion>
    {
        public ExcursionMap()
        {
            Table("Excursion");
            Id(d => d.Id_excursion).Not.Nullable();
            Map(x => x.Name).Not.Nullable().Length(30).Default("Стандартна");
            Map(x => x.Time).Default("CURRENT_TIME");
            Map(x => x.Date_expiration).Default("CURRENT_DATE").Check("Date_expiration >= CURRENT_TIME");
            Map(x => x.Schedule).Length(30).Default("Пн");
            Map(x => x.Cost).Not.Nullable();
            //Inverse() - ставиться на той стороне отношения(не владелец ассоциации), которая будет отвечать за сохранение, но не будет сохранятся 
            HasManyToMany(x => x.Employees)
                .Table("Employee_Excursion").ParentKeyColumn("Id_excursion").ChildKeyColumn("Id_employee")
                .Cascade.All().Inverse();
        }
    }
}