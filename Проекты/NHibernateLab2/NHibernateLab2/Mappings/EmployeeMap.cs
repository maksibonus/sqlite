﻿using FluentNHibernate.Mapping;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Mappings
{
    internal class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Table("Employee");
            Id(d => d.Id_employee).Not.Nullable().Unique();
            Map(x => x.Name).Not.Nullable().Length(40);
            Map(x => x.Salary).Not.Nullable().Check("Salary>0").Default("0");
            Map(x => x.Position).Not.Nullable().Length(20).Default("Екскурсовод");
            Map(x => x.Telephone).Length(15);
            HasMany(x => x.Halls)
                .Cascade.All();
            HasManyToMany(x => x.Excursions)
                .Cascade.All()
                .Table("Employee_Excursion").ParentKeyColumn("Id_employee").ChildKeyColumn("Id_excursion");
        }
    }
}