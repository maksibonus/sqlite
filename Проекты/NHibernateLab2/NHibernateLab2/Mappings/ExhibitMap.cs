﻿using FluentNHibernate.Mapping;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Mappings
{
    internal class ExhibitMap : ClassMap<Exhibit>
    {
        public ExhibitMap()
        {
            Table("Exhibit");
            Id(d => d.Id_exhibit).Not.Nullable();
            Map(x => x.Name).Not.Nullable().Length(30);
            Map(x => x.Technique).Length(20);
            Map(x => x.Date_create).Default("CURRENT_DATE");
            Map(x => x.Author).Not.Nullable().Length(30);
            Map(x => x.Material).Not.Nullable().Length(20);
            References(x => x.Hall).Column("Id_hall");
        }
    }
}