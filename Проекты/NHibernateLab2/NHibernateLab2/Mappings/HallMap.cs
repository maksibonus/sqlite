﻿using FluentNHibernate.Mapping;
using NHibernateLab2.Entities;

namespace NHibernateLab2.Mappings
{
    internal class HallMap : ClassMap<Hall>
    {
        public HallMap()
        {
            Table("Hall");
            Id(d => d.Id_hall).Not.Nullable();
            Map(x => x.Name).Not.Nullable().Length(30);
            Map(x => x.Square).Check("Square>0").Default("1");
            References(x => x.Employee).Column("Id_employee");
            HasMany(x => x.Exhibits).Cascade.All().Inverse();
        }
    }
}