﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NHibernateLab2.Entities
{
    public class Hall
    {
        public Hall()
        {
            Exhibits = new List<Exhibit>();
        }
        [Key]
        public virtual int Id_hall { get; protected set; }

        public virtual string Name { get; set; }
        public virtual float Square { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual IList<Exhibit> Exhibits { get; set; }
    }
}