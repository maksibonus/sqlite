﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NHibernateLab2.Entities
{
    public class Employee
    {
        public Employee()
        {
            Excursions = new List<Excursion>();
            Halls = new List<Hall>();
        }

        [Key]
        public virtual int Id_employee { get; protected set; }

        public virtual string Name { get; set; }
        public virtual decimal Salary { get; set; }
        public virtual string Position { get; set; }
        public virtual string Telephone { get; set; }
        public virtual IList<Excursion> Excursions { get; protected set; }
        public virtual IList<Hall> Halls { get; protected set; }
    }
}