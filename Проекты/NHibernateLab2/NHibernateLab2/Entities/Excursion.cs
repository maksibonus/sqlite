﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NHibernateLab2.Entities
{
    public class Excursion
    {
        public Excursion()
        {
            Employees = new List<Employee>();
        }

        [Key]
        public virtual int Id_excursion { get; protected set; }

        public virtual string Name { get; set; }
        public virtual DateTime Time { get; set; }
        public virtual DateTime Date_expiration { get; set; }
        public virtual string Schedule { get; set; }
        public virtual decimal Cost { get; set; }
        public virtual IList<Employee> Employees { get; set; }
    }
}