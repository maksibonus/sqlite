﻿using System.ComponentModel.DataAnnotations;

namespace NHibernateLab2.Entities
{
    public class Exhibit
    {
        [Key]
        public virtual int Id_exhibit { get; protected set; }

        public virtual Hall Hall { get; set; }
        public virtual string Name { get; set; }
        public virtual string Technique { get; set; }
        public virtual string Date_create { get; set; }
        public virtual string Author { get; set; }
        public virtual string Material { get; set; }
    }
}